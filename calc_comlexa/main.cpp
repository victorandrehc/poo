#include <iostream>
#include "tad.h"
#include <inttypes.h>

using namespace std;

void determinaTermos(const char* expressao, char*primeiroTermo, char*segundoTermo, char* operacao){
	bool isPrimeiroTermo=true;
	bool passouPrimeiroTermo=false; 
	int aux=0;
	uint8_t aSize=strlen(expressao);
	for (uint8_t i=0;i<aSize;i++){
		if(isPrimeiroTermo && passouPrimeiroTermo && (expressao[i]=='*'||expressao[i]=='+'||expressao[i]=='-'||expressao[i]=='/'||expressao[i]=='^')){
			isPrimeiroTermo=false;
			*operacao=expressao[i];
			aux=0;
		}else if(isPrimeiroTermo){
			if(expressao[i]=='p'||expressao[i]=='i'){
				passouPrimeiroTermo=true;
			}
			primeiroTermo[aux]=expressao[i];
			aux++;
		}else if(!isPrimeiroTermo){
			segundoTermo[aux]=expressao[i];
			aux++;
		}
	}
}

int main(int argc, char* argv[]){
	if(argc<2){
		cout<<"ERRO PRECISA DE UM ARGUMENTO\n";
		return -1;
	}else{		
		char* primeiroTermo=new char[10];
		char* segundoTermo=new char[10];
		char operacao;
		determinaTermos(argv[1],primeiroTermo,segundoTermo,&operacao);
		cout<<primeiroTermo<<operacao<<segundoTermo<<endl;
		Calculadora(primeiroTermo,segundoTermo,operacao);
		delete primeiroTermo;
		delete segundoTermo;
		return 0;
	}
}