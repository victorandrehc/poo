#ifndef TAD_H
#define TAD_H

#include <iostream>
#include <string.h>
#include <cmath>
#include <inttypes.h>
#include <stdio.h>
#define PI 3.14159265359
using namespace std;

class NumeroComplexo{
	public:
		//NumeroComplexo();
		//~NumeroComplexo();
		void setConfig(char *termo);

		void setReal(double r);
		double getReal();
		void setImaginario(double i);
		double getImaginario();
		void setNorma(double n);
		double getNorma();
		void setFase(double f);
		double getFase();
		void toPolar();
		void toRectangular();
	protected:
	private:
		double real,imaginario,norma,fase;
		bool containsP(char* termo);
	
};

class Calculadora
{
	public:
		Calculadora(char* primeiroTermo, char* segundoTermo, char operacao);
		//~Calculadora();
	private:
		NumeroComplexo a,b,c;
		void soma();
		void subtracao();
		void produto();
		void razao();
		void exponenciacao();
	
};


#endif