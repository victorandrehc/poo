#include "tad.h"

//NumeroComplexo::NumeroComplexo(){}

void NumeroComplexo::setConfig(char *termo){
	double a,b;	
	if(containsP(termo)){
		sscanf(termo,"%lf%lfp",&norma,&fase);
		fase*=(PI/180.0);
		toRectangular();
	}else{
		sscanf(termo,"%lf%lfi",&real,&imaginario);
		toPolar();
	}
	//cout<<real<<"+"<<imaginario<<"i\n"<<norma<<"|_"<<fase<<endl;
}

bool NumeroComplexo::containsP(char *termo){
	uint8_t size=strlen(termo);
	for(uint8_t i=0;i<size;i++){
		if (termo[i]=='p'){
			return true;
		}
	}
	return false;
}

void NumeroComplexo::toPolar(){
	norma=sqrt(pow(real,2)+pow(imaginario,2));
	fase=atan(imaginario/real);
}
void NumeroComplexo::toRectangular(){
	real=norma*cos(fase);
	imaginario=norma*sin(fase);
}

void NumeroComplexo::setReal(double r){
	real=r;
}
double NumeroComplexo::getReal(){
	return real;
}
void NumeroComplexo::setImaginario(double i){
	imaginario=i;
}
double NumeroComplexo::getImaginario(){
	return imaginario;
}
void NumeroComplexo::setNorma(double n){
	norma=n;
}
double NumeroComplexo::getNorma(){
	return norma;
}
void NumeroComplexo::setFase(double f){
	fase=f;
}
double NumeroComplexo::getFase(){
	return fase;
}

//NumeroComplexo::~NumeroComplexo(){}

Calculadora::Calculadora(char* primeiroTermo, char* segundoTermo, char operacao){
	a.setConfig(primeiroTermo);
	b.setConfig(segundoTermo);
	switch(operacao){
		case '+':
			soma();
			break;
		case '-':
			subtracao();
			break;
		case '*':
			produto();
			break;
		case '/':
			razao();
			break;
		case '^':
			exponenciacao();
			break;
		default:
			break;
	}
	cout<<c.getReal()<<"+"<<c.getImaginario()<<"i\n"<<c.getNorma()<<"|_"<<c.getFase()/(PI/180.0)<<endl;
}
void Calculadora::soma(){
	c.setReal(a.getReal()+b.getReal());
	c.setImaginario(a.getImaginario()+b.getImaginario());
	c.toPolar();
}
void Calculadora::subtracao(){
	c.setReal(a.getReal()-b.getReal());
	c.setImaginario(a.getImaginario()-b.getImaginario());
	c.toPolar();
}
void Calculadora::produto(){
	c.setNorma(a.getNorma()*b.getNorma());
	c.setFase(a.getFase()+b.getFase());
	c.toRectangular();
}
void Calculadora::razao(){
	c.setNorma(a.getNorma()/b.getNorma());
	c.setFase(a.getFase()-b.getFase());
	c.toRectangular();
}
void Calculadora::exponenciacao(){
	c.setNorma(1.0);
	c.setFase(0.0);
	int expoente=abs(b.getReal());
	if(b.getReal()>0){
		for(int i=0;i<expoente;i++){
			c.setNorma(c.getNorma()*a.getNorma());
			c.setFase(c.getFase()+a.getFase());
		}	
	}else if(b.getReal()<0){
		for(int i=0;i<expoente;i++){
			c.setNorma(c.getNorma()/a.getNorma());
			c.setFase(c.getFase()-a.getFase());
		}
	}
	c.toRectangular();

}

//Calculadora::~Calculadora(){}
