#ifndef MATRIX_H_INCLUDED
#define MATRIX_H_INCLUDED
#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <iostream>

const bool debug=false;
class Matrix{
private:
	int n;
	int m;
	double **mat;
	Matrix(){}
	const int getMatrixLine(double **matriz)const;
	const int getMatrixColumn(double **matriz)const;
	void initMat();
	const int getN() const{return n;}
	const int getM() const{return m;}
	const void setN(const int nn) {n=nn;}
	const void setM(const int mm) {m=mm;}
public:
	Matrix(const Matrix &);
	Matrix(double **matriz,const int , const int );
	~Matrix();
	int* size()const;
	Matrix operator+ (const Matrix &matriz) const;
	Matrix operator* (const double k) const;
	
	void print();
	
};
#endif