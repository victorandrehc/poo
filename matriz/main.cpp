#include <iostream>
#include "matrix.h"
using namespace std;


int main(){
	double **m;

	m = new double*[3];
	for(int i=0;i<3;i++)
		m[i] = new double[4];
	for(int i=0;i<3;i++)
		for(int j=0;j<4;j++)
			m[i][j] = (i==j)?1:0;



	Matrix teste(m,3,4);
	Matrix teste2(teste);
	teste2.print();
	cout<<"SOMA\n";
	Matrix t=teste+teste2;
	teste.print();
	cout<<endl;
	teste2.print();
	cout<<endl;
	t.print();
	/*cout<<"MULTIPLICACAO POR CONSTANTE (3)\n";
	t=teste*3;
	teste.print();
	cout<<endl;
	t.print();*/

	return 0;
}