#include "matrix.h"


Matrix::Matrix(double **matriz,const int nn, const int mm){
	
	n=((nn==0)? getMatrixLine(matriz):nn);
	m=((mm==0)? getMatrixColumn(matriz):mm);
	mat=matriz;
	
	if(debug){
		std::cout<<"matriz construida ("<<n<<","<<m<<")\n";
		for(int i=0;i<n;i++){
			for(int j=0;j<m;j++){
				std::cout<<mat[i][j]<<" ";
			}
			std::cout<<"\n";
		}
	}
}
Matrix::Matrix(const Matrix &matriz){
	//std::cout<<"construtor de copia\n";
	m=matriz.getM();
	n=matriz.getN();
	mat = new double*[n];
	for(int i=0;i<n;i++){
		mat[i] = new double[m];
		for(int j=0;j<m;j++){
			mat[i][j]=matriz.mat[i][j];
		}
	}
	if(debug){
		std::cout<<"matriz construida construtor de copia ("<<n<<","<<m<<")\n";
		for(int i=0;i<n;i++){
			for(int j=0;j<m;j++){
				std::cout<<mat[i][j]<<" ";
			}
			std::cout<<"\n";
		}
	}
}
Matrix::~Matrix(){
	if(debug){
		std::cout<<"chamando destrutor\n";
		print();
		std::cout<<"\n";
	}
	try{
    	for(int i=0; i<n;i++){
			delete[] mat[i];
		}
		delete[] mat;
  	} catch (int e)  {
    	std::cout << "An exception occurred. Exception Nr. " << e << '\n';
  	}	
}

void Matrix::initMat(){
	mat = new double*[n];
	for(int i=0;i<n;i++)
		mat[i] = new double[m];
}

const int Matrix::getMatrixLine(double **matriz)const{
	return sizeof(matriz);
}

const int Matrix::getMatrixColumn(double **matriz)const{
	return sizeof(matriz[0]);
}

Matrix Matrix::operator+ (const Matrix &matriz) const{
	if(m!=matriz.getM() || n!=matriz.getN()){
		std::cout<<"a matriz deve ter dimensão compativel\n";
		exit(1);
	}else{
		if(debug){
			std::cout<<"a matriz compativel\n";
		}
		Matrix retorno(matriz);
		for(int i=0;i<n;i++){
			for(int j=0;j<m;j++){
				retorno.mat[i][j]=mat[i][j]+matriz.mat[i][j];
			}
		}
		if(debug){
			std::cout<<"retornando\n";
		}
		return retorno;
	}
}
Matrix Matrix::operator* (const double k) const{
	Matrix retorno;
	retorno.setN(n);
	retorno.setM(m);
	retorno.initMat();
	/*for(int i=0;i<n;i++){
		for(int j=0;j<m;j++){
			retorno.mat[i][j]*=k;
		}
	}*/
	if(debug){
		std::cout<<"retornando\n";
	}
	return retorno;
}
void Matrix::print(){
	for(int i=0;i<n;i++){
		for(int j=0;j<m;j++){
			std::cout<<mat[i][j]<<" ";
		}
		std::cout<<"\n";
	}
}