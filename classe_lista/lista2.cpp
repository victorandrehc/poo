#include <iostream>
using namespace std;

class Lista{
	private:
		int tamanhoLista;
		Lista *proximo;
		int conteudoCelula;
		void criaLista(){
			proximo=new Lista;
			proximo->tamanhoLista=0;
			Lista *aux=proximo;
			for(int i=0;i<tamanhoLista-1;i++){
				aux->proximo=new Lista;
				aux->proximo->tamanhoLista=0;
				aux->proximo->setConteudoCelula(i+1);
				aux=aux->proximo;
			}
			aux->proximo=new Lista;
			aux->proximo->setConteudoCelula(tamanhoLista);
			aux->proximo->proximo=NULL;
		}
	public:
		void setTamanhoLista(int t){
			tamanhoLista=t;
			criaLista();
		}
		void printLista(){
			Lista *i=proximo;
			int c=0;
			while(i->proximo!=NULL){
				cout<<i->proximo->getConteudoCelula()<<" "<<++c<<endl;
				i=i->proximo;
			}
		}
		void setConteudoCelula(int c){
			conteudoCelula=c;
		}
		int getConteudoCelula(void){
			return conteudoCelula;
		}
};

int main(){
	Lista cabeca;
	cabeca.setTamanhoLista(3);
	cabeca.printLista();
	return 0;
}