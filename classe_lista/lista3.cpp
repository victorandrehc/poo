#include <iostream>
using namespace std;

class Lista{
	private:
		int tamanhoLista;
		Lista *proximo;
		int conteudoCelula;

		void criaLista(){
			if(tamanhoLista>0){
				proximo=new Lista;
				proximo->setConteudoCelula(1);
				if(tamanhoLista>1){
					proximo->proximo=new Lista;
					Lista* aux=proximo->proximo;
					for (int i=1;i<tamanhoLista-1;i++){
						aux->setConteudoCelula(i+1);
						aux->proximo=new Lista;
						aux=aux->proximo;
					}
					aux->setConteudoCelula(tamanhoLista);
					aux->proximo=NULL;
				}else{
					proximo->proximo=NULL;
				}
			}else{
				proximo=NULL;
			}
		}
	public:
		void setTamanhoLista(int t){
			tamanhoLista=t;
			criaLista();
		}
		void printLista(){
			Lista *i=proximo;
			int c=0;
			while(i!=NULL){
				cout<<i->getConteudoCelula()<<endl;
				i=i->proximo;
			}
		}
		void setConteudoCelula(int c){
			conteudoCelula=c;
		}
		int getConteudoCelula(void){
			return conteudoCelula;
		}
		int getTamanhoLista(void){
			return tamanhoLista;
		}

};

int main(){
	Lista cabeca;
	cabeca.setTamanhoLista(100);
	cabeca.printLista();
	return 0;
}