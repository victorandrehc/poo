#include <iostream>
using namespace std;

class Lista{
	private:
		int tamanhoLista;
		Lista *proximo;
		int conteudoCelula;

		void criaLista(){
			if(tamanhoLista>0){
				Lista* aux=proximo=new Lista;
				for (int i=0;i<tamanhoLista-1;i++){
					aux->setConteudoCelula(i+1);
					aux=aux->proximo=new Lista;
				}
				aux->setConteudoCelula(tamanhoLista);
				aux->proximo=NULL;				
			}else{
				proximo=NULL;
			}
		}
	public:
		void insere(int novaCelula){
			Lista* i=proximo;
			if(i->getConteudoCelula()<novaCelula){
				while(i->proximo!=NULL && i->proximo->getConteudoCelula()<novaCelula){
					i=i->proximo;
				}
				Lista *novo=new Lista;
				novo->setConteudoCelula(novaCelula);
				novo->proximo=i->proximo;
				i->proximo=novo;
			}else{
				Lista *novo=new Lista;
				novo->setConteudoCelula(novaCelula);
				novo->proximo=proximo;
				proximo=novo;
			}
		}
		void remove(int celulaRemovida){
			Lista* i=proximo;
			if(i->getConteudoCelula()!=celulaRemovida){
				while(i->proximo!=NULL && i->proximo->getConteudoCelula()!=celulaRemovida){
					i=i->proximo;
				}
				if(i->proximo!=NULL){
					Lista *exclui=i->proximo;
					i->proximo=exclui->proximo;
					delete exclui;
				}else{
					cout<<"Impossível encontrar item para remoção\n";
				}
			}else{
				proximo=proximo->proximo;
				delete i;
			}
		}
		void setTamanhoLista(int t){
			tamanhoLista=t;
			criaLista();
		}
		void printLista(){
			cout<<"\nIMPRIMINDO LISTA\n";
			Lista *i=proximo;
			int c=0;
			while(i!=NULL){
				cout<<i->getConteudoCelula()<<endl;
				i=i->proximo;
			}
		}
		void setConteudoCelula(int c){
			conteudoCelula=c;
		}
		int getConteudoCelula(void){
			return conteudoCelula;
		}
		int getTamanhoLista(void){
			return tamanhoLista;
		}

};

int main(){
	Lista cabeca;
	cabeca.setTamanhoLista(10);
	cabeca.printLista();
	cabeca.insere(12);
	cabeca.printLista();
	cabeca.insere(11);
	cabeca.printLista();
	cabeca.insere(-1);
	cabeca.printLista();
	cabeca.insere(0);
	cabeca.printLista();
	cabeca.remove(-1);
	cabeca.printLista();
	cabeca.remove(12);
	cabeca.printLista();
	cabeca.remove(5);
	cabeca.printLista();
	cabeca.remove(5);
	cabeca.printLista();

	return 0;
}