#include <iostream>
using namespace std;

void function(int &i){
	//método interessante para passar por referência
	i=5;
}
void function2(int *i){
	*i=6;
}

int main(){
	int array[3]={0,1,2};
	int *p;
	p=array;
	for (int i = 0; i < 3; ++i)
	{
		cout << *p++<<endl;
	}
	int i=0;
	function(i);
	cout << "Teste de referência: "<<i<<endl;
	function2(&i);
	cout << "Teste de referência2: "<<i<<endl;
	return 0;	
}