
#include <iostream>

using namespace std;

int main(){
	int i;
	double v[3];
	double max,min,meio;
	cin >> i >> v[0] >> v[1] >> v[2];
	max = meio = min = v[0];
	for(int j=0;j<3;j++){
		if(v[j] >= max)
			max = v[j];
		if(v[j] <= min)
			min = v[j];
	}
	for(int j=0; j<3;j++){
		if(v[j] != max && v[j] != min){
			meio = v[j];
			break;
		}
	}

	if(i==1)
		cout << min << " " << meio << " " << max << endl;
	else if(i==2)
		cout << max << " " << meio << " " << min << endl;
	else if(i==3)
		cout << meio << " " << max << " " << min << endl;
	return 0;

}