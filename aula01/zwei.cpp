#include <iostream>
#include <vector>

using namespace std;

void getVector(vector<int>* v){
	int i;
	while(cin >>i && i>=0){		
		v->push_back(i);		
	}
}

int main(){
	vector<int> v;
	getVector(&v);
	int repeteCount=1,repeteValue=v[0];
	for(int i=1;i<v.size();i++){
		if(v[i]==repeteValue){
			repeteCount++;
		}else{
			cout<< repeteCount <<" "<<repeteValue<<" ";
			repeteCount=1;
			repeteValue=v[i];
		}
	
	}
	cout<< repeteCount <<" "<<repeteValue<<endl;
	return 0;
}