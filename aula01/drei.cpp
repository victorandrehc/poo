#include <iostream>
#include <vector>

using namespace std;

void getVector(vector<int>* v){
	int i;
	while(cin >>i && i>=0){		
		v->push_back(i);		
	}
}

int main(){
	vector<int> v;
	getVector(&v);
	int repeteCount=1,repeteValue=v[0];
	for(int i=0;i<v.size();i+=2){
		for(int j=0;j<v[i];j++){
			cout<<v[i+1]<<" ";
		}
	}
	cout<<"\n";
	return 0;
}