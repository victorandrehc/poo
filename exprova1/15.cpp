#include <iostream>
using namespace std;

int func(double a){
	return (int)a;
}
int main(){
	int (*f)(double)=func;
	int (*g)(double);
	g=func;
	cout<<"f="<<(*f)(2.0)<<" g="<<(*g)(3.0)<<endl;
	return 0;
}