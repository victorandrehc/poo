#include <iostream>
using namespace std;

class X
{
public:
	X(double valor=1.0);
	~X();
	X(const X &x);
private:
	double *d;
	
};

X::X(double valor){
	d=new double;
	*d=valor;
}

X::~X(){
	cout<<"chamada do destrutor "<<*d<<endl;
	delete d;
}

X::X(const X &x){
	cout<<"construtor de copia"<<endl;
	d=new double;
	*d=*x.d;
}


X func(X x){
	cout<<"chamada da func\n";
	return x;
}

int main(){
	X x;
	cout<<"declaração do objeto\n";
	X xx=func(x);
	//func(x);
	cout<<"chamada da função\n";
	return 0;
}

// o construtor de cópia default não funciona bem com atributos que são ponteiros
// é necessário, portanto, criar um contrutor de cpopia que coipe os atributos da classe
//O contrutor de cópia é chamado sempre que que um objeto é chamado em uma classe, portanto 
//ele é chamadao quando um objeto é passado por cópia ou é retornado por cópia, no caso da passagem por parametro o destrutor de cópia é sempre chamado
// no retorno, ele é chamado somente se o retorno da função é jogado fora 