#include <iostream>
#include <sstream>
using namespace std;

class Bird{
private:
	string s;
	int i;
	static int id;
public:
	Bird(int n=1);
	Bird( const Bird &b);
	~Bird();
	const Bird operator+ (const Bird &b);
	const Bird operator- (const Bird &b);
	const Bird operator* (const Bird &b);
	const Bird operator/ (const Bird &b);
	friend ostream& operator<< (const Bird &b);
};


int Bird::id=0;
Bird::Bird(int n){
	cout<<"Construtor"<<endl;
	id++;
	stringstream str;
	str<<id;
	name = "Bird#" + str.str();
	i = n;
}
Bird::Bird(const Bird &b){
	cout<<"Construtor de cópia"<<endl;
	id++;
	ostringstream ostr;
	ostr<<id;
	s = "Bird#" + ostr.str();
	i = b.i;
}

const Bird Bird::operator+(const Bird &b) {
	Bird aux(i+b.i);
	return aux;
}

/*const Bird Bird::operator-(const Bird &b) {
	Bird aux(i-b.i);
	return aux;
}
const Bird Bird::operator*(const Bird &b) {
	Bird aux(i+b.i);
	return aux;
}

const Bird Bird::operator/(const Bird &b) {
	Bird aux(i-b.i);
	return aux;
}

ostream& operator<<(const Bird &b){
	ostream aux;
	aux<<"i= "<<b.i<<endl;
	return aux;
}*/

int main(){
	return 0;
}


