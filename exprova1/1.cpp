#include <iostream>
using namespace std;

class Monitor{
	public:
		Monitor();
		~Monitor();
		int getI() const;
		int setI(int ii);
		int incrementI();
	private:
		int i;
	
};
inline Monitor::Monitor(){i=0;}
inline Monitor::~Monitor(){}
inline  int Monitor::getI()const{return i;}
inline  int Monitor::setI(int ii){i=ii;}
inline  int Monitor::incrementI(){i++;}

Monitor m();
void incident(Monitor &a){
	m.incrementI();
}
void print(){
	cout<<m.getI();
}

int main(){
	return 0;
}