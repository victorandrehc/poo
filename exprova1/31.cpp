#include <iostream>
using namespace std;

class X{
private:
	int *i;
	//int a;
public:
	X(int=1);
	~X();
	X(const X &x, int a=1);

};

X::X(int a){
	i=new int;
	*i=a;
}
X::X(const X &x, int a){
	i=new int;
	*i=*(x.i);
	cout<<"construtor de copia "<<a<<"ponteiro-> "<<*i<<endl;
}
X::~X(){
	cout<<"destrutor de copia: "<<*i<<endl;
	delete i;
}
X func(X x){
	cout<<"chamada da func\n";
	return x;
}

int main(){
	X x;
	cout<<"declaração do objeto\n";
	X xx=func(x);
	//func(x);
	cout<<"chamada da função\n";
	return 0;
}