#ifndef _LIB_H_
#define _LIB_H_
#include <iostream>
using namespace std;

class X{
	public:
		X(int ii);
		~X();
		int getI() const;
		int setI(int ii);
		int print();
	private:
		int i;
	
};
inline X::X(int ii){i=ii;}
inline X::~X(){}
inline  int X::getI()const{return i;}
inline  int X::setI(int ii){i=ii;}
//inline  int X::print(){std::cout<<i<<endl;}
#endif
