#include "graph.h"
#include "edge.h"
#include <iostream>
#include <vector>
using namespace std;



bool Graph::GetValorMatriz(const int i,const int j) const{
    return Adjacencia[i][j];

}

Graph::Graph(int Num){
	NumArestas = 0;
	NumVertices = Num;
	Adjacencia = new bool*[Num];
	for(int i=0;i<Num;i++)
		Adjacencia[i] = new bool[Num];
	for(int i=0;i<Num;i++)
		for(int j=0;j<Num;j++)
			Adjacencia[i][j] = FALSE;
}

Graph::~Graph(){
	for(int i=0; i< NumVertices;i++)
		delete[] Adjacencia[i];
	delete[] Adjacencia;
}

bool Graph::insert(const Edge& Aresta){
	if(Aresta.GetPinicial() == Aresta.GetPfinal())
		return FALSE;

	if(Adjacencia[Aresta.GetPinicial()-1][Aresta.GetPfinal()-1] == TRUE){
		return FALSE;
	}
	else{
		NumArestas++;
		Adjacencia[Aresta.GetPinicial()-1][Aresta.GetPfinal()-1] = TRUE;
		Adjacencia[Aresta.GetPfinal()-1][Aresta.GetPinicial()-1] = TRUE;
		return TRUE;
	}
}

bool Graph::remove(const Edge& Aresta){
	if(Aresta.GetPinicial() == Aresta.GetPfinal())
		return FALSE;

	if(Adjacencia[Aresta.GetPinicial()-1][Aresta.GetPfinal()-1] == FALSE){
		return FALSE;
	}
	else{
		NumArestas--;
		Adjacencia[Aresta.GetPinicial()-1][Aresta.GetPfinal()-1] = FALSE;
		Adjacencia[Aresta.GetPfinal()-1][Aresta.GetPinicial()-1] = FALSE;
		return TRUE;
	}
}

int Graph::GetNumArestas() const{
	return NumArestas;
}

int Graph::GetNumVertices() const{
	return NumVertices;
}

bool Graph::edge(const Edge& Aresta) const{
	if(Adjacencia[Aresta.GetPinicial()-1][Aresta.GetPfinal()-1] == TRUE)
		return TRUE;

	return FALSE;
}

void Graph::Imprime() const{
	cout << endl;
	for(int i = 0;i<NumVertices;i++){
		for(int j = 0;j<NumVertices;j++){
			cout << Adjacencia[i][j] << ' ';
		}
		cout << endl;
	}
}

bool Graph::Completo() const{
	for(int i = 0;i<NumVertices;i++){
		for(int j = 0;j<NumVertices;j++){
			if(!Adjacencia[i][j] && i != j)
				return FALSE;
		}
	}
	return TRUE;
}

void Graph::Completa(){
	for(int i = 0;i<NumVertices;i++){
		for(int j = 0;j<NumVertices;j++){
			if(i != j)
				Adjacencia[i][j] = TRUE;
		}
	}
}

int* Graph::Largura(const int vertice,const bool print) const{
	int distanciaAtual =1;
	int* percorridos = new int[NumVertices];
	int verticeAtual = vertice-1;

	for(int i = 0;i<NumVertices;i++)
		percorridos[i] = 0;
    if(print)
        cout << "Vertices em ordem pela busca em largura: ";

	while(distanciaAtual < NumVertices){

		for(int i = 0;i<NumVertices;i++){

			if(distanciaAtual!= 1 && percorridos[i] == distanciaAtual-1 && i!=vertice-1){
				verticeAtual=i;
				for(int j = 0;j<NumVertices;j++){
					if(GetValorMatriz(j,verticeAtual)){
						if(percorridos[j] == 0  && j!=vertice-1){
                            if(print)
                                cout << j+1 <<" ";
							percorridos[j] = distanciaAtual;
						}
					}
				}
			}
            else if(distanciaAtual==1){
                for(int j = 0;j<NumVertices;j++){
                    if(GetValorMatriz(j,verticeAtual)){
                        if(print)
                            cout << j+1 <<" ";
                        percorridos[j] = distanciaAtual;

                    }
                }
                distanciaAtual++;
            }
		}
		distanciaAtual++;
	}
	if(print)
        cout << endl;
	return percorridos;
}

void Graph::Profundidade(const int vertice) const{
    Profundidade(vertice,NULL);
    cout << endl;
}

void Graph::Profundidade(const int vertice,bool* percorridos, int NumChamadas) const{
    if (NumChamadas > NumVertices)
        return;
    NumChamadas++;

    if(percorridos == NULL){
        cout << "Vertices em ordem pela busca em profundidade: ";
        percorridos = new bool[NumVertices];
        for(int i = 0; i<NumVertices;i++){
            if(i != vertice-1)
                percorridos[i] = FALSE;
            else
                percorridos[i] = TRUE;
        }
    }

    for(int i=0;i<NumVertices;i++){
        if(GetValorMatriz(i,vertice-1) && !percorridos[i]){
            percorridos[i] = TRUE;
            cout << i+1 << ' ';
            Profundidade(i+1,percorridos,NumChamadas);
        }
    }

}

int Graph::NumConectados(const int vertice) const{
    int *largura = Largura(vertice,0);
    int Num=1;

    for(int i=0;i<NumVertices;i++){
        if(largura[i] != 0)
            Num++;
    }
    return Num;

}


dijkstra* Graph::Dijkstra(const Edge& Aresta) const{
	
	int count, minDistance ,nextNode;
	int distance[NumVertices], pred[NumVertices],visited[NumVertices];

	for(int j=0;j<NumVertices;j++){
		distance[j]=getPeso(Aresta.GetPinicial()-1,j);
		pred[j]=Aresta.GetPinicial()-1;
		visited[j]=0;
	}
	distance[Aresta.GetPinicial()-1]=0;
	visited[Aresta.GetPinicial()-1]=1;
	count=1;
	while(count<NumVertices-1){
		minDistance=inf;
		for(int i=0;i<NumVertices;i++){
			if(distance[i]<minDistance && !visited[i]){
				minDistance=distance[i];
				nextNode=i;
			}
		}
		visited[nextNode]=1;
		for(int i=0; i<NumVertices; i++){
			if(!visited[i] && minDistance+getPeso(nextNode,i)<distance[i]){
				distance[i]=minDistance+getPeso(nextNode,i);
				pred[i]=nextNode;
			}
		} 
		count++;
	}

    int i = Aresta.GetPfinal()-1;
 
    if(distance[i]==inf){
        return NULL;
    }else{		
		dijkstra *retorno= new dijkstra;
		retorno->tamanho=distance[i];
		retorno->vertices=new int[NumVertices];
		retorno->vertices[0]=i+1;

		int j = i,aux=1;
		do{
			j=pred[j];
			retorno->vertices[aux++]=j+1;
			
		}while(j!=Aresta.GetPinicial()-1);

		return retorno;
    }
 
}

void printCaminho(const dijkstra& d){
	if(&d!=NULL){
		std::cout<<"O caminho desejado possui distancia de "<<d.tamanho<<endl;
		int len=sizeof(d.vertices)+1;
		//std::cout<<"len= "<<len<<endl;
		for(int i=len;i>=0;i--){
			if(d.vertices[i]!=0){
				std::cout<<d.vertices[i]<<" ";
			}
		}
		std::cout<<endl;
		
	}else{
		std::cout<<"Nao existe caminho para o grafo desejado";
	}	
}
