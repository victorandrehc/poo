#ifndef GRAPH_H_INCLUDED
#define GRAPH_H_INCLUDED
#include "edge.h"

long const inf=100000;

typedef struct dijkstra{
    int tamanho;
    int* vertices;
} dijkstra;

void printCaminho(const dijkstra&);


class Graph{
	private:
		int NumArestas;
		int NumVertices;
		bool** Adjacencia;
	public:
		Graph(int );
		~Graph();
		bool insert(const Edge& );
		bool remove(const Edge& );
		int GetNumArestas() const;
		int GetNumVertices() const;
		bool edge(const Edge& ) const;
		void Imprime() const;
		bool Completo() const;
		void Completa();
		int* Largura(const int , const bool =1) const ;
		void Profundidade(const int ) const;
		void Profundidade(const int ,bool* ,int = 0 ) const;
		bool GetValorMatriz(const int ,const int ) const;
		int NumConectados(const int) const;
		dijkstra* Dijkstra(const Edge& ) const;

		//void Dijkstra(int vFinal,int vInicial=1);
		int getPeso (int,int)const;
		

};

inline int Graph::getPeso (int i, int j)const{
	return (Adjacencia[i][j]>0) ? i+j+2:inf;
}

#endif // GRAPH_H_INCLUDED
