#include "graph.h"
#include "edge.h"

#include <iostream>

using namespace std;

int main(){
	Graph grafo(9);
	Edge Aresta;

	Aresta.SetInicial(1);
	Aresta.SetFinal(2);
	grafo.insert(Aresta);

	Aresta.SetInicial(2);
	Aresta.SetFinal(4);
	grafo.insert(Aresta);

	Aresta.SetInicial(2);
	Aresta.SetFinal(5);
	grafo.insert(Aresta);

	Aresta.SetInicial(1);
	Aresta.SetFinal(3);
	grafo.insert(Aresta);

	Aresta.SetInicial(3);
	Aresta.SetFinal(6);
	grafo.insert(Aresta);

	Aresta.SetInicial(3);
	Aresta.SetFinal(7);
	grafo.insert(Aresta);

	Aresta.SetInicial(5);
	Aresta.SetFinal(6);
	grafo.insert(Aresta);

    Aresta.SetInicial(8);
	Aresta.SetFinal(9);
	grafo.insert(Aresta);

    Aresta.SetInicial(5);
	Aresta.SetFinal(3);

    dijkstra* teste = grafo.Dijkstra(Aresta);
    printCaminho(*teste);
    //grafo.Dijkstra(Aresta);
	return 0;
}
