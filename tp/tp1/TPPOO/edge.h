#ifndef EDGE_H_INCLUDED
#define EDGE_H_INCLUDED
#define FALSE 0
#define TRUE 1
class Edge{
	private:
		int Pinicial;
		int Pfinal;
	public:
		Edge();
		~Edge();
		int GetPinicial() const;
		int GetPfinal() const;
		void SetInicial(int i);
		void SetFinal(int i);
};
inline int Edge::GetPinicial() const{
	return Pinicial;
}

inline int Edge::GetPfinal() const{
	return Pfinal;
}

inline Edge::Edge(){}

inline Edge::~Edge(){}

inline void Edge::SetInicial(int i){
	Pinicial = i;
}

inline  void Edge::SetFinal(int i){
	Pfinal = i;
}

#endif // EDGE_H_INCLUDED