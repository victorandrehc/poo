
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <sys/time.h>
#include "mat.cpp"

int main()
{   


/*matrix 15x15*/
 int i,j,n=15,m=15;
 double entrada[][15] = {
       { 1201, 924, 346, 1122, 348, 924, 112, 1423, 1205, 969, 1067, 1166, 977, 658, 1778},
{ 1564, 537, 1278, 1321, 1071, 967, 958, 939, 1304, 1005, 1069, 867, 312, 531, 822},
{ 1309, 1275, 1191, 1515, 1112, 905, 981, 1079, 890, 1416, 1509, 557, 1115, 571, 1689},
{ 1075, 1333, 1301, 1432, 1441, 1424, 931, 851, 896, 849, 1306, 927, 702, 1554, 218},
{ 577, 774, 1309, 1301, 907, 795, 686, 875, 1627, 1811, 493, 585, 1289, 1154, 1214},
{ 338, 741, 464, 1025, 1530, 1233, 1448, 915, 915, 940, 1233, 440, 1777, 758, 724},
{ 1052, 335, 1237, 308, 893, 150, 975, 862, 1361, 1138, 674, 1408, 624, 930, 1475},
{ 985, 555, 1424, 938, 973, 1155, 1672, 1158, 253, 1010, 1081, 738, 1319, 1262, 1158},
{ 1289, 103, 618, 477, 1023, 1627, 968, 1148, 1399, 1309, 1302, 1004, 740, 306, 456},
{ 1174, 1297, 524, 699, 1049, 581, 1006, 643, 635, 1518, 724, 1017, 783, 658, 1209},
{ 803, 1575, 1361, 1550, 453, 457, 940, 598, 149, 1540, 1526, 1074, 1156, 551, 308},
{ 1664, 1361, 1148, 1094, 620, 1675, 471, 973, 1621, 680, 1269, 930, 1024, 1652, 1260},
{ 1865, 897, 906, 751, 1105, 626, 599, 1720, 957, 1197, 187, 1334, 767, 1665, 1688},
{ 1805, 1308, 667, 830, 1060, 1661, 918, 1500, 562, 1170, 716, 823, 1068, 1529, 1185},
{ 684, 850, 643, 882, 356, 427, 638, 1752, 1650, 1538, 1206, 1540, 1404, 1547, 1231},

    };


    double **li=(double**)malloc(sizeof(double)*n);    
    double ** in=(double**)malloc(sizeof(double)*n);
    for(i=0;i<n;i++){
        li[i]=(double*)malloc(sizeof(double)*m);
        in[i]=(double*)malloc(sizeof(double)*m);
    }
    for(i=0;i<n;i++){
        for(j=0;j<m;j++){
            in[i][j]=entrada[i][j];
           // printf("%lf ",in[i][j]);
        }
        //printf("\n");
    }
    
    mat_t R, Q;
    mat_t x ;
    x= matrix_copy(in, n, m);
    printf("x:\n");
    matrix_show(&x);

    clock_t start = clock(), diff;

    householder(x, &R, &Q);
    mat_t inv=resolve_LI(Q,R,n);
   
    diff=clock()-start;

    puts("Q");
    matrix_show(&Q);
    puts("R");
    matrix_show(&R);
    puts("inv"); 
    matrix_show(&inv);
    mat_t p=matrix_mul(x,inv);
    printf("x*inv:\n");
    matrix_show(&p);
    // to show their product is the input matrix
    //close(entrada);
    //matrix_delete(&x);
    //matrix_delete(&R);
    //matrix_delete(&Q);
    //matrix_delete(&m);
    free(in);
    free(li);
    printf("\nO sistema demorou %i ciclos de clock e %lf us \n para inverter uma matriz de ordem %i",diff,(double)diff/50.0,n);
    //printf("\ninicial: %f \n ",(double)inicial.tv_sec +(double)inicial.tv_usec/1000000);
   // printf("\nfinal: %f \n ",(double)final.tv_sec +(double)final.tv_usec/1000000 );
    //printf("\ntempo total de inversao : %lf s\n ",(double)final.tv_sec +(double)final.tv_usec/1000000 - (double)inicial.tv_sec +(double)inicial.tv_usec/1000000);
    return 0;
}