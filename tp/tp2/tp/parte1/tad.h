#ifndef TAD_H_INCLUDED
#define TAD_H_INCLUDED

#include <iostream>
#include <cmath>
#include <stdio.h>
#include <vector>
#include <fstream>
using namespace std;

const double pi=3.14159265359;
const double erro=1*pow(10,-5);

class Ponto{
private:
	double x,y,z;
public:
	Ponto(double xx=0.0, double yy=0.0, double zz=0.0){
		x=xx;
		y=yy;
		z=zz;
	}
	Ponto(const Ponto& p){
		x=p.x;
		y=p.y;
		z=p.z;
	}
	double getX()const{return x;};
	double getY()const{return y;};
	double getZ()const{return z;};
	void setX(double xx){x=xx;};
	void setY(double yy){y=yy;};
	void setZ(double zz){z=zz;};

	Ponto operator+(const Ponto& p)const;
	Ponto operator-(const Ponto& p)const;
	Ponto operator/(const double p)const;
	//Ponto& operator=(const Ponto& p)const;
	friend ostream& operator << (ostream& op, const Ponto& p);
	friend istream& operator >> (istream& op,  Ponto& p);
	//~Ponto();
	
};

double distancia (const Ponto&,const Ponto&);
inline double distancia(const Ponto& a, const Ponto&b){
	return sqrt(pow(a.getX()-b.getX(),2)+pow(a.getY()-b.getY(),2)+pow(a.getZ()-b.getZ(),2));
}
Ponto pontoMedio (const Ponto&,const Ponto&);
inline Ponto pontoMedio(const Ponto& a, const Ponto& b){
	return (a+b)/2.0; 
}
	

class Forma{
	public:
		Forma(){};
		virtual ~Forma(){};
		virtual void move(const Ponto& )=0;
		virtual void desenha()=0;
		virtual double area()=0;
		virtual double volume()=0;
		virtual bool pontoNaForma(const Ponto&)=0;
		virtual const char* tipo() const=0;
	    //operadores
		friend ostream& operator << (ostream& op, const Forma& f);
		friend istream& operator >> (istream& op,  Forma& f);
	protected:
		
		//metodos
		virtual void le(istream&)=0;
	    virtual void escreve(ostream&) const=0;	
};

class Forma1D: public Forma{
	public:
		Forma1D(){};
		virtual ~Forma1D(){};
		double area(){return 0;};
		double volume(){return 0;};
		virtual const char* tipo() const{return "Forma1D";};

		virtual void move(const Ponto& )=0;
		virtual void desenha()=0;
		virtual bool pontoNaForma(const Ponto&)=0;
	protected:
		virtual void le(istream&)=0;
		virtual void escreve(ostream& op) const=0;
};

class Circulo: public Forma1D{
	private:
		double r;
		Ponto centro;
	public:
		Circulo(const Ponto&,double=1);
		Circulo(double=1);
		~Circulo();
		const char* tipo() const{return "Circulo";};
		void move(const Ponto& );
		void desenha(){cout<<"desenhando Circulo "<<*this<<endl;};
		bool pontoNaForma (const Ponto& p){
			//cout<<"dist: "<<distancia(p,centro)<<"r: "<<r<<"z: "<<centro.getZ()<<" "<<p.getZ()<<endl;
			return (abs(distancia(p,centro)-r)<=erro && centro.getZ()==p.getZ());
		};
	protected:
		void le(istream&);
		void escreve(ostream& op) const;	
};

class Linha:public Forma1D{
	private:
		Ponto a,b;
	public:
		Linha(const Ponto&, const Ponto&);
		Linha(){};
		~Linha();
		const char* tipo() const{return "Linha";};
		void move(const Ponto& );
		void desenha(){cout<<"desenhando Linha "<<*this<<endl;};
		bool pontoNaForma (const Ponto& p){
			//cout<<"ap: "<<distancia(a,p)<<" pb: "<<distancia(p,b)<<" ab: "<<distancia(a,b)<<endl;
			return (distancia(a,p)+distancia(p,b)==distancia(a,b));
		};
	protected:
		void le(istream&);
		void escreve(ostream& op) const;
	
};

inline void Linha::move(const Ponto& p){
	b = b - a + p;
    a = p;
    cout<<"movendo linha para "<<p<<endl;
}
class Forma2D:public Forma{
	public:
			Forma2D(){};
			virtual ~Forma2D(){};
			double volume(){return 0;};
			virtual const char* tipo() const{return "Forma2D";};

			virtual double area(){};
			virtual void move(const Ponto& )=0;
			virtual void desenha()=0;
			virtual bool pontoNaForma(const Ponto&)=0;
		protected:
			virtual void le(istream&)=0;
			virtual void escreve(ostream& op) const=0;
};
class Disco:public Forma2D{
	private:
		double r;
		Ponto centro;
	public:
		Disco(const Ponto&,double=1);
		Disco(double=1);
		~Disco(){};
		const char* tipo() const{return "Disco";};
		double area(){return pi*r*r;};
		void move(const Ponto&);
		void desenha(){cout<<"desenhando Disco "<<*this<<endl;};
		bool pontoNaForma (const Ponto& p){
			//cout<<"dist: "<<distancia(p,centro)<<"r: "<<r<<"z: "<<centro.getZ()<<" "<<p.getZ()<<endl;
			return (distancia(p,centro)-r<=erro && centro.getZ()==p.getZ());
		};
	protected:
		void le(istream&);
		void escreve(ostream& op) const;	
};

class Vetor{
	double *vec;
	int tam;
public:
	Vetor(const Ponto&,const Ponto&);
	Vetor(const double, const double,const double);
	Vetor(const Ponto&);
	Vetor(const Vetor&);
	~Vetor();
	//friend bool  combLinearPossivel(Vetor, Vetor,Vetor);
	Vetor operator*(const Vetor& p)const;
	double operator/(const Vetor& p)const;//multiplicacao escalar
	friend ostream& operator << (ostream& op, const Vetor& v);
	const double modulo(){
		return sqrt(pow(vec[0],2)+pow(vec[1],2)+pow(vec[2],2));
	}
};
//double** multiplicaMatriz(double**A,double**B,int nLinhas, int nColunas, int ig);

class Retangulo: public Forma2D{
	private:
		Ponto a,b,c,d;
	public:
		Retangulo(const Ponto& aa=Ponto(0,0),const Ponto& bb=Ponto(0,1),const Ponto& cc=Ponto(1,1),const Ponto& dd=Ponto(1,0)){
			a=aa;
			b=bb;
			c=cc;
			d=dd;
		}
		//Retangulo(){}
		~Retangulo(){};
		const char* tipo() const{return "Retangulo";};
		double area(){
			Vetor v(a,b);
			Vetor w(a,d);
			//cout<<"V: "<<v<<"W"<<w;
			Vetor vw=v*w;
			//out<<" VW"<<vw<<endl;
			return vw.modulo(); 
		};
		void move(const Ponto& p){
			a=a+p;
			b=b+p;
			c=c+p;
			d=d+p;
		};
		void desenha(){cout<<"desenhando Retangulo "<<*this<<endl;};
		bool pontoNaForma (const Ponto& p);
	protected:
		void le(istream&);
		void escreve(ostream& op) const;	
};

class Triangulo: public Forma2D{
	private:
		Ponto a,b,c;
	public:
		 Triangulo(const Ponto& aa=Ponto(0,0),const Ponto& bb=Ponto(0,3),const Ponto& cc=Ponto(4,0)){
		 	a=aa;
		 	b=bb;
		 	c=cc;
		 }
		 //Triangulo(){}
		~Triangulo(){}
		const char* tipo() const{return "Triangulo";};
		double area();
		void move(const Ponto& p){
			a=a+p;
			b=b+p;
			c=c+p;
		};
		void desenha(){cout<<"desenhando Triangulo "<<*this<<endl;};
		bool pontoNaForma (const Ponto&);
	protected:
		void le(istream&);
		void escreve(ostream& op) const;		
};
class Forma3D:public Forma{
	public:
			Forma3D(){};
			virtual ~Forma3D(){};
			virtual const char* tipo() const{return "Forma3D";};

			virtual double volume()=0;
			virtual double area(){};
			virtual void move(const Ponto& )=0;
			virtual void desenha()=0;
			virtual bool pontoNaForma(const Ponto&)=0;
		protected:
			virtual void le(istream&)=0;
			virtual void escreve(ostream& op) const=0;
};
class Esfera: public Forma3D{
	private:
		double r;
		Ponto centro;
	public:
		Esfera(const Ponto& p,double rr=1){
			centro=p;
			r=r;
		}
		Esfera(double rr=1){
			r=rr;
		}
		~Esfera(){};
		
		const char* tipo() const{return "Esfera";};
		double volume(){return (4.0/3.0)*pi*pow(r, 3);};
		double area(){return 4.0*pi*pow(r, 2);};
		void move(const Ponto& p){centro=p;};
		void desenha(){cout<<"desenhando Esfera "<<*this<<endl;};
		bool pontoNaForma (const Ponto& p){
			//cout<<"dist: "<<distancia(p,centro)<<" r: "<<r<<" "<<(distancia(p,centro)-r)<<endl;
			return (distancia(p,centro)-r<=erro);
		};
	protected:
		void le(istream&);
		void escreve(ostream& op) const;	
		
};
class Cubo : public Forma3D{
	private:
		Ponto origem;
		double lado;
	public:
	    ~Cubo(){};
	    Cubo(int l=1){lado=l;};
	    Cubo(Ponto& p,double l){
	    	origem=p;
	    	lado=l;
	    };
		const char* tipo() const{return "Cubo";};
		double volume(){return pow(lado,3);};
		double area(){return 6.0*pow(lado, 2);};
		void move(const Ponto& p){origem=p;};
		void desenha(){cout<<"desenhando Cubo "<<*this<<endl;};
		bool pontoNaForma (const Ponto& p){return (p.getX() >= origem.getX() && p.getX() <= origem.getX() + lado) &&
												  (p.getY() >= origem.getY() && p.getY() <= origem.getY() + lado) &&
												  (p.getZ() >= origem.getZ() && p.getZ() <= origem.getZ() + lado) ;};
	protected:
		void le(istream&);
		void escreve(ostream& op) const;	
};

class FormaGenerica{
private:
public:
	vector <Forma*> formas;
	FormaGenerica(){}
	~FormaGenerica();
	void desenha(int i) const{
		if(i<formas.size()){
			formas[i]->desenha();			
		}
	};
	void move(Ponto &p, int i) const{
		if(i<formas.size()){
			formas[i]->move(p);
		}
	}
	void area(int i) const{
		if(i<formas.size()){
			cout<<formas[i]->tipo()<<" "<<i<<":"<<formas[i]->area()<<endl;			
		}
	};
	void volume(int i) const{
		if(i<formas.size()){
			cout<<formas[i]->tipo()<<" "<<i<<":"<<formas[i]->volume()<<endl;			
		}
	};
	void desenha() const;
	void move(Ponto &) const;
	void area() const;
	void volume() const;
	void escreveFile(ofstream &file);
	/*void move_uma(const ponto&, int i);
	void move_todas(const ponto&);
	void area_todas() const;
	void volume_todas() const;
	void area_uma(const ponto&) const;
	void volume_uma(const ponto&) const;*/
};



#endif