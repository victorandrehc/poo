#ifndef MAT_H_INCLUDED
#define MAT_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <sys/time.h>

typedef struct {
    int m, n;
    double ** v;
} mat_t;

mat_t matrix_new(int m, int n);
void matrix_delete(mat_t* m);
void matrix_transpose(mat_t *m);
mat_t matrix_copy(double **a, int m, int n);
mat_t matrix_mul(mat_t x, mat_t y);
void matrix_show(mat_t* m);
mat_t matrix_minor(mat_t x, int d);
/* c = a + b * s */
double *vmadd(double a[], double b[], double s, double c[], int n);
/* m = I - v v^T */
mat_t vmul(double v[], int n);
/* ||x|| */
double vnorm(double x[], int n);
/* y = x / d */
double* vdiv(double x[], double d, double *y, int n);
/* take c-th column of m, put in v */
double* mcol(mat_t m, double *v, int c);
void householder(mat_t m, mat_t *R, mat_t *Q);
double somatorio_solucao(mat_t a, double*x, int linha, int n);
mat_t resolve_LI(mat_t Q, mat_t R,int n);


#endif