#include "tad.h"




ostream & operator <<(ostream& op, const Ponto& p){
	op<<"("<<p.x<<", "<<p.y<<", "<<p.z<<")";
	return op;
}
istream& operator >> (istream& op,  Ponto& p){
	op>>p.x>>p.y>>p.z;
}

Ponto Ponto::operator+(const Ponto& p)const{
	return Ponto(p.x+x,p.y+y,p.z+z);
}
Ponto Ponto::operator-(const Ponto& p)const{
	return Ponto(x-p.x,y-p.y,z-p.z);
}
Ponto Ponto::operator/(const double p)const{
	return Ponto(x/p,y/p,z/p);
}


ostream & operator <<(ostream& op, const Forma& f){
	f.escreve(op);
	return op;
}
istream & operator >>(istream& op, Forma& f){
	f.le(op);
	return op;
}


void Circulo::escreve(ostream & op) const{
	op <<"centro: "<<centro<<" raio: "<<r;	
}
void Circulo::le(istream& op){
	double x,y,z;
	cout<<"Lendo circulo\n";
	op>>x>>y>>z>>r;
	centro.setX(x);
	centro.setY(y);
	centro.setZ(z);
	//cout<<*this;
}
/*bool Circulo::pontoNaForma(const Ponto& p){
	cout<<"dist: "<<distancia(p,centro)<<"r: "<<r<<"z: "<<centro.getZ()<<" "<<p.getZ()<<endl;
	return (distancia(p,centro)==r && centro.getZ()==p.getZ());
}*/
void Circulo::move(const Ponto& p){
	centro=p;
	cout<<"movendo circulo para "<<centro<<endl;
} 
Circulo::Circulo(const Ponto& p,double rr){
	centro=p;
	r=rr;
	//cout<<"criando circulo: "<<*this<<endl;
}
Circulo::Circulo(double rr){
	r=rr;	
	//cout<<"criando circulo: "<<*this<<endl;
}

Circulo::~Circulo(){
}

Linha::Linha(const Ponto& aa, const Ponto& bb){
	a=aa;
	b=bb;
	cout<<"criando Linha: "<<*this<<endl;
}
Linha::~Linha(){}
void Linha::escreve(ostream & op) const{
	op <<"ponto inicial: "<<a<<" ponto final: "<<b;	
}
void Linha::le(istream& op){
	double ax,ay,az,bx,by,bz;
	cout<<"Lendo Linha\n";
	op>>ax>>ay>>az>>bx>>by>>bz;
	a.setX(ax);
	a.setY(ay);
	a.setZ(az);
	b.setX(bx);
	b.setY(by);
	b.setZ(bz);
	//cout<<*this;
}

void Disco::escreve(ostream & op) const{
	op <<"centro: "<<centro<<" raio: "<<r;	
}
void Disco::le(istream& op){
	double x,y,z;
	cout<<"Lendo disco\n";
	op>>x>>y>>z>>r;
	centro.setX(x);
	centro.setY(y);
	centro.setZ(z);
	//cout<<*this;
}
/*bool Circulo::pontoNaForma(const Ponto& p){
	cout<<"dist: "<<distancia(p,centro)<<"r: "<<r<<"z: "<<centro.getZ()<<" "<<p.getZ()<<endl;
	return (distancia(p,centro)==r && centro.getZ()==p.getZ());
}*/
void Disco::move(const Ponto& p){
	centro=p;
	cout<<"movendo disco para "<<centro<<endl;
}

Disco::Disco(const Ponto& p,double rr){
	centro=p;
	r=rr;
	//cout<<"criando disco: "<<*this<<endl;
}
Disco::Disco(double rr){
	r=rr;	
	//cout<<"criando disco: "<<*this<<endl;
}

Vetor::Vetor(const Ponto& a,const Ponto& b){
	vec = new double [3];
	Ponto p=a-b;
	vec[0]=p.getX();
	vec[1]=p.getY();
	vec[2]=p.getZ();
	//cout<<"criando vetor de "<<a<<" e "<<b<<": "<<*this<<endl;
}
Vetor::Vetor(const double i, const double j,const double k){
	vec = new double [3];
	vec[0]=i;
	vec[1]=j;
	vec[2]=k;
	//cout<<"criando vetor "<<*this<<endl;
}
Vetor::Vetor(const Ponto& p){
	vec = new double [3];
	vec[0]=p.getX();
	vec[1]=p.getY();
	vec[2]=p.getZ();
}
Vetor::Vetor(const Vetor& p){
	vec = new double [3];
	int i;
	for(i=0;i<3;i++){
		vec[i]=p.vec[i];
	}
	
}
Vetor::~Vetor(){
	delete vec;
}
Vetor Vetor::operator*(const Vetor& w)const{
	return Vetor((vec[1]*w.vec[2]-vec[2]*w.vec[1]),
					(vec[2]*w.vec[0]-vec[0]*w.vec[2]),
					(vec[0]*w.vec[1]-vec[1]*w.vec[0]));
}
double Vetor::operator/(const Vetor& p)const{//multiplicação escalar
	return vec[0]*p.vec[0]+p.vec[1]*vec[1]+vec[2]*p.vec[2];
}
ostream & operator <<(ostream& op, const Vetor& v){
	op<<"("<<v.vec[0]<<", "<<v.vec[1]<<", "<<v.vec[2]<<")";
	return op;
}

void Retangulo::escreve(ostream & op) const{
	op <<"a: "<<a<<" b: "<<b<<" c: "<<c<<" d: "<<d;	
}
void Retangulo::le(istream& op){
	double ax,ay,az;
	double bx,by,bz;
	double cx,cy,cz;
	double dx,dy,dz;

	cout<<"Lendo Retangulo\n";
	op>>ax>>ay>>az>>bx>>by>>bz>>cx>>cy>>cz>>dx>>dy>>dz;
	a.setX(ax);
	a.setY(ay);
	a.setZ(az);	
	b.setX(bx);
	b.setY(by);
	b.setZ(bz);	
	c.setX(cx);
	c.setY(cy);
	c.setZ(cz);
	d.setX(dx);
	d.setY(dy);
	d.setZ(dz);
	//cout<<*this;
}

bool Retangulo::pontoNaForma(const Ponto& p){
	Triangulo apb (a,p,b);
	Triangulo bpc (b,p,c);
	Triangulo cpd (c,p,d);
	Triangulo apd (a,p,d);
	//cout<<"apb "<<apb.area()<<" bpc "<<bpc.area()<<" cpd "<<cpd.area()<<"apd "<<apd.area()<<" abcd "<<area()<<endl;
	//double aux=apb.area()+bpc.area()+apc.area();
	//cout<<"areas "<<aux<<" "<<area()<<" "<<(aux==area())<< endl;
	return (apb.area()+bpc.area()+cpd.area()+apd.area()==area());
}

void Triangulo::escreve(ostream & op) const{
	op <<"a: "<<a<<" b: "<<b<<" c: "<<c;	
}
void Triangulo::le(istream& op){
	double ax,ay,az;
	double bx,by,bz;
	double cx,cy,cz;

	cout<<"Lendo Triangulo\n";
	op>>ax>>ay>>az>>bx>>by>>bz>>cx>>cy>>cz;
	a.setX(ax);
	a.setY(ay);
	a.setZ(az);	
	b.setX(bx);
	b.setY(by);
	b.setZ(bz);	
	c.setX(cx);
	c.setY(cy);
	c.setZ(cz);

	//cout<<*this;
}
double Triangulo::area(){
	Vetor v(b,a);
	Vetor w(c,a);
	Vetor vw=v*w;
	return vw.modulo()/2.0;
	//double p=0.5*(distancia(a,b)+distancia(b,c)+distancia(c,a));
	//return sqrt(p*(p-distancia(a,b))*(p-distancia(b,c))*(p-distancia(c,a)));		
}

bool Triangulo::pontoNaForma(const Ponto& p){
	Triangulo apb (a,p,b);
	Triangulo bpc (b,p,c);
	Triangulo apc (a,p,c);
	//cout<<"apb "<<apb.area()<<" bpc "<<bpc.area()<<" apc "<<apc.area()<<" abc "<<area()<<endl;
	//double aux=apb.area()+bpc.area()+apc.area();
	//cout<<"areas "<<aux<<" "<<area()<<" "<<(aux==area())<< endl;
	return (apb.area()+bpc.area()+apc.area()==area());
}

void Esfera::escreve(ostream & op) const{
	op <<"centro: "<<centro<<" raio: "<<r;	
}
void Esfera	::le(istream& op){
	double x,y,z;
	cout<<"Lendo esfera\n";
	op>>x>>y>>z>>r;
	centro.setX(x);
	centro.setY(y);
	centro.setZ(z);
	//cout<<*this;
}

void Cubo::escreve(ostream & op) const{
	op <<"origem: "<<origem<<" lado: "<<lado;	
}
void Cubo::le(istream& op){
	double x,y,z;
	cout<<"Lendo cubo\n";
	op>>x>>y>>z>>lado;
	origem.setX(x);
	origem.setY(y);
	origem.setZ(z);
	//cout<<*this;
}

FormaGenerica::~FormaGenerica(){
    for(int i = (int)formas.size() - 1; i >= 0; i--)
    {
        //cout << "Destruindo "<<formas[i]->tipo()<<" "<<*formas[i]<<endl;
        delete formas[i];
    }
}

void FormaGenerica::desenha() const{
	for(int i=0; i < (int)formas.size(); i++){
		formas[i]->desenha();
	}
}
void FormaGenerica::move(Ponto & p) const{
	for(int i=0; i < (int)formas.size(); i++){
		formas[i]->move(p);
	}
}

void FormaGenerica::area() const{
	for(int i=0; i < (int)formas.size(); i++){
		cout<<formas[i]->tipo()<<" "<<i<<":"<<formas[i]->area()<<endl;
	}
}
void FormaGenerica::volume() const{
	for(int i=0; i < (int)formas.size(); i++){
		cout<<formas[i]->tipo()<<" "<<i<<":"<<formas[i]->volume()<<endl;
	}
}
void FormaGenerica::escreveFile(ofstream &file){
	for(int i=0; i < (int)formas.size(); i++){
		//cout<<*formas[i]<<endl;
		file<<formas[i]->tipo()<<" "<<i<<":"<<*formas[i]<<endl;
	}
}