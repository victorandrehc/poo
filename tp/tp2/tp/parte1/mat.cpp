#include "mat.h"
/*para compilar execute sparc-elf-g++ -msoft-float matrix_leon.cpp -o matrix -lm*/
/*site de gabarito http://www.bluebit.gr/matrix-calculator/*/




mat_t matrix_new(int m, int n)
{
    mat_t * x = (mat_t*)malloc(sizeof(mat_t));
    x->v = (double**) malloc(sizeof(double) * m);
    x->v[0] = (double*) calloc(sizeof(double), m * n);
    int i;
    for (i = 0; i < m; i++)
        x->v[i] = x->v[0] + n * i;
    x->m = m;
    x->n = n;
    return *x;
}

void matrix_delete(mat_t* m)
{
    free(m->v[0]);
    free(m->v);
    free(m);
}

void matrix_transpose(mat_t *m)
{
    int i,j;
    for ( i = 0; i < m->m; i++) {
        for ( j = 0; j < i; j++) {
            double t = m->v[i][j];
            m->v[i][j] = m->v[j][i];
            m->v[j][i] = t;
        }
    }
}

mat_t matrix_copy(double **a, int m, int n)
{
    int i, j;
    mat_t x= matrix_new(m, n);
    for (i = 0; i < m; i++)
        for ( j = 0; j < n; j++)
            x.v[i][j] = a[i][j];
    return x;
}
mat_t matrix_mul(mat_t x, mat_t y)
{
    mat_t r;
    if (x.n != y.m){
        printf("erro na multiplicacao\n");
         exit(1);
         //return r;
    }else{
        int i,j,k;
        r = matrix_new(x.m, y.n);
        for ( i = 0; i < x.m; i++)
            for ( j = 0; j < y.n; j++)
                for ( k = 0; k < x.n; k++)
                    r.v[i][j] += x.v[i][k] * y.v[k][j];
            return r;
    }
}

void matrix_show(mat_t* m)
{
    int i, j;
    for( i = 0; i < m->m; i++) {
        for ( j = 0; j < m->n; j++) {
            printf(" %8.3f", m->v[i][j]);
        }
        printf("\n");
    }
    printf("\n");
}

mat_t matrix_minor(mat_t x, int d)
{
    int i,j;
    mat_t m = matrix_new(x.m, x.n);
    for (i = 0; i < d; i++)
        m.v[i][i] = 1;
    for ( i = d; i < x.m; i++)
        for ( j = d; j < x.n; j++)
            m.v[i][j] = x.v[i][j];
    return m;
}

/* c = a + b * s */
double *vmadd(double a[], double b[], double s, double c[], int n)
{
    int i;
    for ( i = 0; i < n; i++)
        c[i] = a[i] + s * b[i];
    return c;
}

/* m = I - v v^T */
mat_t vmul(double v[], int n)
{
    int i,j;
    mat_t x = matrix_new(n, n);
    for ( i = 0; i < n; i++)
        for ( j = 0; j < n; j++)
            x.v[i][j] = -2 *  v[i] * v[j];
    for ( i = 0; i < n; i++)
        x.v[i][i] += 1;

    return x;
}
/* ||x|| */
double vnorm(double x[], int n)
{
    double sum = 0;
    int i;
    for ( i = 0; i < n; i++)
        sum += x[i] * x[i];
    return sqrt(sum);
}
/* y = x / d */
double* vdiv(double x[], double d, double *y, int n)
{
    int i;
    for (i = 0; i < n; i++)
        y[i] = x[i] / d;
    return y;
}

/* take c-th column of m, put in v */
double* mcol(mat_t m, double *v, int c)
{
    int i;
    for ( i = 0; i < m.m; i++)
        v[i] = m.v[i][c];
    return v;
}

void householder(mat_t m, mat_t *R, mat_t *Q)
{
    mat_t q[m.m];
    mat_t z = m, z1;
    int k;
   for ( k = 0; k < m.n && k < m.m - 1; k++) {
        double e[m.m], x[m.m], a;
        z1 = matrix_minor(z, k);
        /*if (memcmp(&z,&m,sizeof(mat_t))) {
            matrix_delete(&z);
        }*/
        z = z1;
        mcol(z, x, k);
        a = vnorm(x, m.m);
        if (m.v[k][k] > 0){
            a=-a;
        }


        int i;
        for (i = 0; i < m.m; i++)
            e[i] = (i == k) ? 1 : 0;

        vmadd(x, e, a, e, m.m);
        vdiv(e, vnorm(e, m.m), e, m.m);
        q[k] = vmul(e, m.m);
        z1 = matrix_mul(q[k], z);
        /*if (memcmp(&z,&m,sizeof(mat_t))) {
            matrix_delete(&z);
        }*/
        z = z1;

    }
    //matrix_delete(&z);
    *Q = q[0];

    *R = matrix_mul(q[0], m);
    int i;
    for (i = 1; i < m.n && i < m.m - 1; i++) {
        z1 = matrix_mul(q[i], *Q);
        if (i > 1) {
            //matrix_delete(*Q);
        }
        *Q = z1;
        //matrix_delete(q[i]);
    }
    //matrix_delete(q[0]);
    z = matrix_mul(*Q, m);
    //matrix_delete(*R);
    *R = z;
    matrix_transpose(Q);
}
double somatorio_solucao(mat_t a, double*x, int linha, int n){
    int i;
    double sum=0;
    for(i=linha;i<(n-1);i++){
        sum+=a.v[linha][i+1]*x[i+1];
    }
    return -1*sum;
}

mat_t resolve_LI(mat_t Q, mat_t R,int n){
    int i, j,k;
    double** inversa=(double**)malloc(sizeof(double)*n);
    double **li=(double**)malloc(sizeof(double)*n);
    for(i=0;i<n;i++){
        li[i]=(double*)malloc(sizeof(double)*n);
         inversa[i]=(double*)malloc(sizeof(double)*n);
    }
    double * resp;
    resp=(double*)malloc(sizeof(double)*n);
    

    matrix_transpose(&Q);
    
    for(k=0;k<n;k++){
        for(i=0;i<n;i++){
            for(j=0;j<n;j++){
                if(i==k){
                    li[i][j]=1;
                }
                else{
                    li[i][j]=0;
                }
        }
        }
        for(i=0;i<n;i++){
            resp[i]=0;
        }
        mat_t LI=matrix_copy(&li[0],Q.n,1);
        LI=matrix_mul(Q,LI);
        for(i=n;i>0;i--){
            inversa[i-1][k]=(1/R.v[i-1][i-1])*(LI.v[0][i-1]+somatorio_solucao(R,resp,i-1,n));
            resp[i-1]=inversa[i-1][k];
        }
    }
    mat_t inv=matrix_copy(inversa,n,n);
    free(inversa);
    free(li);
    return inv;
}