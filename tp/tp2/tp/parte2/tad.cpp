#include "tad.h"


void Seq::print(ostream & op)const{
	op<<"[";
	for (int i=0;i<seq.size();i++){
		op<<seq[i];
		if(i<seq.size()-1){
			op<<",";
		}
	}
	op<<"]";
}
void Seq::print(int i, int j)const{
	if(j==-1){
		j=seq.size();
	}else if(j>seq.size()||j<i||i<0){
		throw SeqErr("Indice de impressao invalido\n");
	}
	cout<<tipo()<<": [";
	i--;
	for (;i<j;i++){
		cout<<seq[i];
		if(i<j-1){
			cout<<",";
		}
	}
	cout<<"]\n";	
}

ostream& operator << (ostream& op, const Seq& s){
	s.print(op);
	return op;
}

unsigned long int Seq::elem(int i){
	//cout<<"elemento pedido "<<i<<" "<<seq.size()<<endl;
	if(i>seq.size()){
		gen_elems(i);
		return seq[i-1];
	}else if(i>0){
		return seq[i-1];
	}else{
		throw SeqErr("Indice invalido\n");
	}
}

void Seq::gen_elems(int i){
	//cout<<"gerando elementos "<<i<<endl;
	if(seq.size()==0){
		seq.push_back(genFirst());
		gen_elems(i);
	}else if(seq.size()==1){
		seq.push_back(genSecond());
		gen_elems(i);
	}else{
		for (int j = seq.size(); j < i; j++){ //Gera os elementos até o iésimo
			seq.push_back(gen(j));
		}
	}
	
}


Fibonnacci::Fibonnacci(int i){
	//seq.push_back(1);
	//seq.push_back(1);
	cout<<"construtor\n";
	elem(i);
}
unsigned long int Fibonnacci::gen(int j){
	return seq[j-1] + seq[j-2];
}

Lucas::Lucas(int i){
	//seq.push_back(1);
	//seq.push_back(3);
	elem(i);
}

unsigned long int Lucas::gen(int j){
	return seq[j-1] + seq[j-2];
}

Pell::Pell(int i){
	//seq.push_back(1);
	//seq.push_back(2);
	elem(i);
}
unsigned long Pell::gen(int j){
	return 2*seq[j-1]+seq[j-2];
}

Triangular::Triangular(int i){
	//seq.push_back(1);
	elem(i);
}
unsigned long int Triangular::gen(int j){
	return seq[j-1]+j+1;
}

Quadrados::Quadrados(int i){
	elem(i);
}

unsigned long int Quadrados::gen(int j){
	return (j+1)*(j+1);
}

Pentagonal::Pentagonal(int i){
	elem(i);
}

unsigned long int Pentagonal::gen(int j){
	return (j+1)*(3*j+2)/2;
}


Container::~Container(){
	for(int i=0;i<v.size();i++){
		delete v[i];
	}
}
void Container::print(int i, int j)const{
	for(int k=0;k<v.size();k++){
		try{
				v[k]->print(i,j);
		}catch(SeqErr &e){
			e.out();
		}
	}		
}
