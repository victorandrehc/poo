#include <iostream>
#include "tad.h"
using namespace std;

/*int main(){
	Fibonnacci fib;
	cout<<"Fibbonacci\n"<<fib<<endl;
	cout<<fib.elem(8)<<" ";
	cout<<fib.elem(4)<<endl;
	cout<<fib<<endl;

	Lucas lucas;
	cout<<"\nLucas\n"<<lucas<<endl;
	cout<<lucas.elem(8)<<" ";
	cout<<lucas.elem(4)<<endl;
	cout<<lucas<<endl;

	Pell pell;
	cout<<"\npell\n"<<pell<<endl;
	cout<<pell.elem(8)<<" ";
	cout<<pell.elem(4)<<endl;
	cout<<pell<<endl;

	Triangular triangular;
	cout<<"\ntriangular\n"<<triangular<<endl;
	cout<<triangular.elem(8)<<" ";
	cout<<triangular.elem(4)<<endl;
	cout<<triangular<<endl;

	Quadrados quadrados;
	cout<<"\nquadrados\n"<<quadrados<<endl;
	cout<<quadrados.elem(8)<<" ";
	cout<<quadrados.elem(4)<<endl;
	cout<<quadrados<<endl;

	Pentagonal pentagonal;
	cout<<"\npentagonal\n"<<pentagonal<<endl;
	cout<<pentagonal.elem(8)<<" ";
	cout<<pentagonal.elem(4)<<endl;
	cout<<pentagonal<<endl;

	cout<<"PRINT PARTE\n";
	fib.print();
	try{
		pentagonal.print(4,8);
	}catch (SeqErr &e){
		e.out();
	}
	return 0;
}*/
int main(){
	Container c;
	c.v.push_back(new Fibonnacci());
	c.v.push_back(new Lucas());
	c.v.push_back(new Pell());
	c.v.push_back(new Triangular());
	c.v.push_back(new Quadrados());
	c.v.push_back(new Pentagonal());	
	c.print();
	cout<<"\nCOM INICIALIZACAO\n";
	c.v.push_back(new Fibonnacci(8));
	c.v.push_back(new Lucas(8));
	c.v.push_back(new Pell(8));
	c.v.push_back(new Triangular(8));
	c.v.push_back(new Quadrados(8));
	c.v.push_back(new Pentagonal(8));
	c.print();
	cout<<"\nPARCIAL\n";
	c.print(1,4);
	
	return 0;
}