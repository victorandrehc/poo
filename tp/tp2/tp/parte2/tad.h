#ifndef TAD_H_INCLUDED
#define TAD_H_INCLUDED
#include <iostream>
#include <vector>
using namespace std;

class SeqErr{
	string err;
public:
	SeqErr(string e){err=e;};
	void out(){cout<<err<<endl;}
	//~Err();
};

class Seq{
	protected:
		virtual unsigned long int gen(int)=0;
		virtual unsigned long int genFirst()=0;
		virtual unsigned long int genSecond()=0;
		virtual void gen_elems(int);
		vector <unsigned long int> seq;
	public:
		Seq(){};
		virtual ~Seq(){
			//cout<<"chamando destrutor\n";
			seq.clear();
		};
		unsigned long int elem(int=0);
		void print(ostream &)const;
		void print (int=1, int=-1)const;
		int length(){return seq.size();}

		virtual const char* tipo()const=0;
		
		friend ostream& operator << (ostream& op, const Seq& s);
};

class Fibonnacci:public Seq{
	protected:
		unsigned long int gen(int);
		unsigned long int genFirst(){return 1;}
		unsigned long int genSecond(){return 1;;}
	public:
		Fibonnacci(int);
		Fibonnacci(){};
		const char* tipo()const{return "Fibonnacci";}
		//~Fibonnacci();
	
};

class Lucas :public Seq{
	protected:
		unsigned long int gen(int);
		unsigned long int genFirst(){return 1;}
		unsigned long int genSecond(){return 3;}
	public:
		Lucas(int);
		Lucas(){};
		const char* tipo()const{return "Lucas";}
		//~Lucas();
};
class Pell :public Seq{
	protected:
		unsigned long int gen (int);
		unsigned long int genFirst(){return 1;}
		unsigned long int genSecond(){return 2;}
	public:
		Pell(int);
		Pell(){};
		const char* tipo()const{return "Pell";}
		//~Pell();	
};

class Triangular:public Seq{
	protected:
		unsigned long int gen(int);
		unsigned long int genFirst(){return 1;}
		unsigned long int genSecond(){return 3;}
	public:
		Triangular(int);
		Triangular(){};
		const char* tipo()const{return "Triangular";}
		//~Triangular();	
};

class Quadrados:public Seq{
	protected:
		unsigned long int gen(int);
		unsigned long int genFirst(){return 1;}
		unsigned long int genSecond(){return 4;}
	public:
		Quadrados(int);
		Quadrados(){};
		const char* tipo()const{return "Quadrados";}
		//~Quadrados();	
};
class Pentagonal: public Seq{
	protected:
		unsigned long int gen(int);
		unsigned long int genFirst(){return 1;}
		unsigned long int genSecond(){return 5;}
	public:
		Pentagonal(int);
		Pentagonal(){};
		const char* tipo()const{return "Pentagonal";}
		//~Pentagonal();		
};

class Container{
	private:
	public:
		//Container();
		~Container();
		vector<Seq*> v;
		void print(int=1,int=-1) const;

	
};


#endif