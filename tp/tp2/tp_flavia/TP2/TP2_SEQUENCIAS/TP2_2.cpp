#include <iostream>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include "TP2_2.h"

using namespace std;

//Imprime quaisquer das sequ�ncias at� o ponto em que elas est�o criadas
void operator<< (ostream& saida, Seq& s){
    s.print(saida);
}

void printPedaco(int i, int f, Seq& s){
    s.imprimeParte(i, f);
}

void printParte(int inicio, int fim, vector<Seq*> s){
    int i;
    for(i=0; i<(int)s.size(); i++){
       printPedaco(inicio, fim, *s[i]);
       cout << endl;
    }
}

//Imprime todas as sequ�ncias de forma completa
void printAll(vector<Seq*> v){
    int i;
    for(i=0; i<(int)v.size(); i++)
        cout << *v[i]; //Usando o operador sobrecarregado
}

//Auxilia na impress�o de um elemento de todas as sequ�ncias
void printElem(int elem, Seq& s){
    cout << s.elem(elem);
}

//Auxilia na impress�o de um elemento de todas as sequ�ncias
void printElemento(int elem, vector<Seq*> s){
    int i;
    for(i=0; i<(int)s.size(); i++){ //Percorre o vetor de sequ�ncias
        printElem(elem, *s[i]); //Passa o elemento e um objeto como par�metros
        cout << endl;
    }
}

//container
container::container(){
    cout << "Construindo container!" << endl;
}
container::~container(){
    int aux = (int)sequencias.size()-1;
    int i;
    for(i=aux; i>=0; i--) //Percorre o vetor de sequ�ncias do fim para o come�o
        delete sequencias[i]; //Deleta os objetos
    cout << "Destruindo container!" << endl;
}

//Seq
Seq::Seq(){ //Construtor
    cout << "Construindo Seq!" << endl;
}

Seq::~Seq(){ //Destrutor
    cout << "Destruindo Seq!" << endl;
}

//S�rie de Fibonacci
Fibonacci::Fibonacci(int i){ //Construtor
    cout << "Contruindo Fibonacci!" << endl;

    f.push_back(1); //f[0]
    f.push_back(1); //f[1]

    gen_elems(i); //Gerar i elementos da s�rie
}

Fibonacci::~Fibonacci(){ //Destrutor
    cout << "Destruindo Fibonacci!" << endl;
    f.clear(); //Deleta o vetor que guarda a s�rie
}

int Fibonacci::length(){
    return f.size(); //Retorna o tamanho atual da s�rie
}

void Fibonacci::gen_elems(int i){ //Gera i elementos da s�rie de Fibonacci
    int j, prox;
    if(i > (int)f.size()){ //Se a quantidade de elementos no vetor for menor do que o elemento que se queira
        int aux = f.size();
        for (j = aux; j < i; j++){ //Gera os elementos at� o i�simo
            prox = f[j-1] + f[j-2];
            f.push_back(prox);
        }
    }
}

unsigned long int Fibonacci::elem(int i){ //Retorna o i-�simo elemento da s�rie de Fibonacci
    cout << "Fibonacci:";
    if(i > length()) //Se o n�mero requisitado ainda n�o estiver no vetor
        gen_elems(i);
    return f[i-1]; //Retorna o i�simo elemento
}

ostream& Fibonacci::print(ostream &os){
    int j;
    cout << "Fibonacci: ";
    for(j = 0; j < length(); j++) //Percorre o vetor
        os << f[j] << " "; //Imprime os elementos
    cout << endl;
    return os;
}

void Fibonacci::imprimeParte(int inicio, int fim){
    int j;
    cout << "Fibonacci: ";
    if(fim > length()) //Gera mais elementos se fim > tamanho atual da s�rie
        gen_elems(fim);
    for(j = inicio-1; j < fim; j++) //Imprime a faixa de elementos requisitada
        cout << f[j] << " ";
}

//S�rie de Lucas
Lucas::Lucas(int i){ //Construtor
    cout << "Contruindo Lucas!" << endl;

    L.push_back(1); //L[0]
    L.push_back(3); //L[1]

    gen_elems(i);
}

Lucas::~Lucas(){ //Destrutor
    cout << "Destruindo Lucas!" << endl;
    L.clear();
}

int Lucas::length(){
    return L.size(); //Retorna o tamanho atual da s�rie
}

void Lucas::gen_elems(int i){
    int j, prox;
    if(i > length()){
        int aux = length();
        for (j = aux; j < i; j++){
            prox = L[j-1] + L[j-2];
            L.push_back(prox);
        }
    }
}

unsigned long int Lucas::elem(int i){ //Retorna o i-�simo elemento da s�rie de Lucas
    cout << "Lucas:";
    if(i > length()) //Se o n�mero requisitado ainda n�o estiver no vetor
        gen_elems(i);
    return L[i-1];  //Retorna o i�simo elemento
}

ostream& Lucas::print(ostream &os){
    int j;
    cout << "Lucas: ";
    for(j = 0; j < length(); j++) //Percorre o vetor
        os << L[j] << " "; //Imprime os elementos
    cout << endl;
    return os;
}

void Lucas::imprimeParte(int inicio, int fim){
    int j;
    cout << "Lucas: ";
    if(fim > length()) //Gera mais elementos se fim > tamanho atual da s�rie
        gen_elems(fim);
    for(j = inicio-1; j < fim; j++) //Imprime a faixa de elementos requisitada
        cout << L[j] << " ";
}

//S�rie de Pell
Pell::Pell(int i){ //Construtor
    cout << "Contruindo Pell!" << endl;

    P.push_back(1); //P[0]
    P.push_back(2); //P[1]

    gen_elems(i);
}

Pell::~Pell(){ //Destrutor
    cout << "Destruindo Pell!" << endl;
    P.clear();
}

int Pell::length(){
    return P.size(); //Retorna o tamanho atual da s�rie
}

void Pell::gen_elems(int i){
    int j, prox;

    if(i > length()){
        int aux = length();
        for (j = aux; j < i; j++){
            prox = 2*P[j-1] + P[j-2];
            P.push_back(prox);
        }
    }
}

unsigned long int Pell::elem(int i){ //Retorna o i-�simo elemento da s�rie de Pell
    cout << "Pell:";
    if(i > length()) //Se o n�mero requisitado ainda n�o estiver no vetor
        gen_elems(i);
    return P[i-1];  //Retorna o i�simo elemento
}

ostream& Pell::print(ostream &os){
    int j;
    cout << "Pell: ";
    for(j = 0; j < length(); j++) //Percorre o vetor
        os << P[j] << " "; //Imprime os elementos
    cout << endl;
    return os;
}

void Pell::imprimeParte(int inicio, int fim){
    int j;
    cout << "Pell: ";
    if(fim > length()) //Gera mais elementos se fim > tamanho atual da s�rie
        gen_elems(fim);
    for(j = inicio-1; j < fim; j++) //Imprime a faixa de elementos requisitada
        cout << P[j] << " ";
}

//S�rie Triangular
Triangular::Triangular(int i){ //Construtor
    cout << "Contruindo Triangular!" << endl;

    t.push_back(1); //t[0]

    gen_elems(i);
}

Triangular::~Triangular(){ //Destrutor
    cout << "Destruindo Triangular!" << endl;
    t.clear();
}

int Triangular::length(){
    return t.size(); //Retorna o tamanho atual da s�rie
}

void Triangular::gen_elems(int i){
    int j, prox;

    if(i > length()){
        int aux = length();
        for (j = aux; j < i; j++){
            prox = t[j-1] + j + 1;
            t.push_back(prox);
        }
    }
}

unsigned long int Triangular::elem(int i){ //Retorna o i-�simo elemento da s�rie Triangular
    cout << "Triangular:";
    if(i > length()) //Se o n�mero requisitado ainda n�o estiver no vetor
        gen_elems(i);
    return t[i-1]; //Retorna o i�simo elemento
}

ostream& Triangular::print(ostream &os){
    int j;
    cout << "Triangular: ";
    for(j = 0; j < length(); j++) //Percorre o vetor
        os << t[j] << " "; //Imprime os elementos
    cout << endl;
    return os;
}

void Triangular::imprimeParte(int inicio, int fim){
    int j;
    cout << "Triangular: ";
    if(fim > length()) //Gera mais elementos se fim > tamanho atual da s�rie
        gen_elems(fim);
    for(j = inicio-1; j < fim; j++) //Imprime a faixa de elementos requisitada
        cout << t[j] << " ";
}

//S�rie Quadrados
Quadrados::Quadrados(int i){ //Construtor
    cout << "Contruindo Quadrados!" << endl;

    gen_elems(i);
}

Quadrados::~Quadrados(){ //Destrutor
    cout << "Destruindo Quadrados!" << endl;
    q.clear();
}

int Quadrados::length(){
    return q.size(); //Retorna o tamanho atual da s�rie
}

void Quadrados::gen_elems(int i){
    int j, prox;

    if(i > length()){
        int aux = length();
        for (j = aux; j < i; j++){
            prox = (j+1)*(j+1);
            q.push_back(prox);
        }
    }
}

unsigned long int Quadrados::elem(int i){ //Retorna o i-�simo elemento da s�rie Quadrados
    cout << "Quadrados:";
    if(i > length()) //Se o n�mero requisitado ainda n�o estiver no vetor
        gen_elems(i);
    return q[i-1]; //Retorna o i�simo elemento
}

ostream& Quadrados::print(ostream &os){
    int j;
    cout << "Quadrados: ";
    for(j = 0; j < length(); j++) //Percorre o vetor
        os << q[j] << " "; //Imprime os elementos
    cout << endl;
    return os;
}

void Quadrados::imprimeParte(int inicio, int fim){
    int j;
    cout << "Quadrados: ";
    if(fim > length()) //Gera mais elementos se fim > tamanho atual da s�rie
        gen_elems(fim);
    for(j = inicio-1; j < fim; j++) //Imprime a faixa de elementos requisitada
        cout << q[j] << " ";
}

//S�rie Pentagonal
Pentagonal::Pentagonal(int i){ //Construtor
    cout << "Contruindo Pentagonal!" << endl;

    gen_elems(i);
}

Pentagonal::~Pentagonal(){ //Destrutor
    cout << "Destruindo Pentagonal!" << endl;
    p.clear();
}

int Pentagonal::length(){
    return p.size(); //Retorna o tamanho atual da s�rie
}

void Pentagonal::gen_elems(int i){
    int j, prox;

    if(i > length()){
        int aux = length();
        for (j = aux; j < i; j++){
            prox = (j+1)*(3*j+2)/2;
            p.push_back(prox);
        }
    }
}

unsigned long int Pentagonal::elem(int i){ //Retorna o i-�simo elemento da s�rie Pentagonal
    cout << "Pentagonal:";
    if(i > length()) //Se o n�mero requisitado ainda n�o estiver no vetor
        gen_elems(i);
    return p[i-1]; //Retorna o i�simo elemento
}

ostream& Pentagonal::print(ostream &os){
    int j;
    cout << "Pentagonal: ";
    for(j = 0; j < length(); j++) //Percorre o vetor
        os << p[j] << " "; //Imprime os elementos
    cout << endl;
    return os;
}

void Pentagonal::imprimeParte(int inicio, int fim){
    int j;
    cout << "Pentagonal: ";
    if(fim > length()) //Gera mais elementos se fim > tamanho atual da s�rie
        gen_elems(fim);
    for(j = inicio-1; j < fim; j++) //Imprime a faixa de elementos requisitada
        cout << p[j] << " ";
}
