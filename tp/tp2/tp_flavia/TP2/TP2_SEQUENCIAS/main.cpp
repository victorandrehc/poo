#include <iostream>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include "TP2_2.h"

using namespace std;

int main()
{
    int opcao = 0;
    int sequen = 0;
    int qtde = 0;
    int inicio = 0;
    int fim = 0;
    int elemento = 0;
    container s;

    while (opcao != 5){
        cout << "\nEscolha uma opcao:" << endl;
        cout << "1 - Escolher uma nova sequencia" << endl;
        cout << "2 - Imprimir todas as sequencias armazenadas de forma completa" << endl;
        cout << "3 - Imprimir uma faixa de valores de todas as sequencia" << endl;
        cout << "4 - Imprimir um i-esimo elemento de todas as sequencias" << endl;
        cout << "5 - Sair" << endl;
        cin >> opcao;

        switch (opcao){
        case 1:
            cout << "Escolha um tipo de sequencia:" << endl;
            cout << "1 - Fibonacci" << endl;
            cout << "2 - Lucas" << endl;
            cout << "3 - Pell" << endl;
            cout << "4 - Triangular" << endl;
            cout << "5 - Quadrados" << endl;
            cout << "6 - Pentagonal" << endl;
            cin >> sequen;

            switch(sequen){
            case 1:
                cout << "Informe a quantidade de elementos dessa sequencia que deseja gerar: ";
                cin >> qtde;
                s.sequencias.push_back(new Fibonacci(qtde)); //Cria novo obejto e adiciona no vetor de sequências
                break;

            case 2:
                cout << "Informe a quantidade de elementos dessa sequencia que deseja gerar: ";
                cin >> qtde;
                s.sequencias.push_back(new Lucas(qtde)); //Cria novo obejto e adiciona no vetor de sequências
                break;

            case 3:
                cout << "Informe a quantidade de elementos dessa sequencia que deseja gerar: ";
                cin >> qtde;
                s.sequencias.push_back(new Pell(qtde)); //Cria novo obejto e adiciona no vetor de sequências
                break;

            case 4:
                cout << "Informe a quantidade de elementos dessa sequencia que deseja gerar: ";
                cin >> qtde;
                s.sequencias.push_back(new Triangular(qtde)); //Cria novo obejto e adiciona no vetor de sequências
                break;

            case 5:
                cout << "Informe a quantidade de elementos dessa sequencia que deseja gerar: ";
                cin >> qtde;
                s.sequencias.push_back(new Quadrados(qtde)); //Cria novo obejto e adiciona no vetor de sequências
                break;

            case 6:
                cout << "Informe a quantidade de elementos dessa sequencia que deseja gerar: ";
                cin >> qtde;
                s.sequencias.push_back(new Pentagonal(qtde)); //Cria novo obejto e adiciona no vetor de sequências
                break;
            }
            break;

        case 2:
            printAll(s.sequencias);
            break;

        case 3:
            cout << "Informe o primeiro elemento da sequencia a ser impresso: ";
            cin >> inicio;
            cout << endl;
            cout << "Informe o ultimo elemento da sequencia a ser impresso: ";
            cin >> fim;
            cout << endl;
            printParte(inicio, fim, s.sequencias);
            break;

        case 4:
            cout << "Informe o elemento das sequencias a ser impresso: ";
            cin >> elemento;
            printElemento(elemento, s.sequencias);
            break;
        }
    }
    return 0;
}
