#ifndef TP2_2_H_INCLUDED
#define TP2_2_H_INCLUDED

using namespace std;

class Seq{
    protected:
        virtual void gen_elems(int i) = 0; //Gerar i elementos

    public:
        Seq(); //Construtor
        virtual ~Seq(); //destrutor
        virtual unsigned long int elem(int i) = 0; //Retorna o i�simo elemento
        virtual ostream& print(ostream& ) = 0; //Imprime a sequ�ncia inteira
        virtual int length() = 0; //retorna o tamanho da sequ�ncia
        virtual void imprimeParte (int , int ) = 0; //Imprime uma faixa de valores da sequ�ncia
        friend void operator<< (ostream& , Seq& ); //Sobrecarga do operador <<

        friend void printParte(int, int, vector<Seq*> ); //Auxilia na impress�o de uma faixa de valores das sequ�ncias
        friend void printPedaco(int, int, Seq& ); //Auxilia na impress�o de uma faixa de valores das sequ�ncias
        friend void printAll (vector<Seq*> ); //Imprime todas as sequ�ncias de forma completa
        friend void printElemento (int, vector<Seq*> ); //Auxilia na impress�o de um elemento de todas as sequ�ncias
        friend void printElem(int, Seq& ); //Auxilia na impress�o de um elemento de todas as sequ�ncias
};

class container{
    public:
        container(); //Construtor
        ~container(); //Destrutor
        vector<Seq*> sequencias; //Vetor de sequ�ncias
};

class Fibonacci : public Seq{
    vector<unsigned long int> f; //Guarda a sequ�ncia

    public:
        Fibonacci(int=0);
        ~Fibonacci();
        void gen_elems(int i);
        unsigned long int elem(int i);
        ostream& print(ostream& os);
        int length();
        void imprimeParte (int inicio, int fim);
};

class Lucas : public Seq{
    vector <unsigned long int> L; //Guarda a sequ�ncia

    public:
        Lucas(int i=0);
        ~Lucas();
        void gen_elems(int i);
        unsigned long int elem(int i);
        ostream& print(ostream& os);
        int length();
        void imprimeParte (int inicio, int fim);
};

class Pell : public Seq{
    vector <unsigned long int> P; //Guarda a sequ�ncia

    public:
        Pell(int i=0);
        ~Pell();
        void gen_elems(int i);
        unsigned long int elem(int i);
        ostream& print(ostream& os);
        int length();
        void imprimeParte (int inicio, int fim);
};

class Triangular : public Seq{
    vector <unsigned long int> t; //Guarda a sequ�ncia

    public:
         Triangular(int i=0);
        ~Triangular();
        void gen_elems(int i);
        unsigned long int elem(int i);
        ostream& print(ostream& os);
        int length();
        void imprimeParte (int inicio, int fim);
};

class Quadrados : public Seq{
    vector <unsigned long int> q; //Guarda a sequ�ncia

    public:
        Quadrados(int i=0);
        ~Quadrados();
        void gen_elems(int i);
        unsigned long int elem(int i);
        ostream& print(ostream& os);
        int length();
        void imprimeParte (int inicio, int fim);
};

class Pentagonal : public Seq{
    vector <unsigned long int> p; //Guarda a sequ�ncia

    public:
        Pentagonal(int i=0);
        ~Pentagonal();
        void gen_elems(int i);
        unsigned long int elem(int i);
        ostream& print(ostream& os);
        int length();
        void imprimeParte (int inicio, int fim);
};

#endif // TP2_2_H_INCLUDED
