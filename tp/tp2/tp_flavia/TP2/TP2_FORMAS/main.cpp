#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <vector>
#include <fstream>
#include "TP2_1.h"

using namespace std;

int main()
{
    Minhas_formas X;
    int n = 0, escolha = 0, erro;
    ponto p, p0;

    while(1)
    {
        cout << "\nDigite:\n";
        cout << "1 para criar uma nova forma\n";
        cout << "2 para desenhar todas as formas\n";
        cout << "3 para mover uma forma\n";
        cout << "4 para mover todas as formas\n";
        cout << "5 para calcular a area de uma forma\n";
        cout << "6 para calcular o volume de uma forma\n";
        cout << "7 para calcular a area de todas as formas\n";
        cout << "8 para calcular o volume de todas as formas\n";
        cout << "9 para deletar todas e sair\n\n";

        cin >> escolha;

        if(escolha == 1)
        {
            erro = 0;
            string nome;
            cout << "\nQual forma vai ser criada?\n";
            cin >> nome;
            if(nome == "linha")
                X.formas.push_back(new Linha);
            else if(nome == "circulo")
                X.formas.push_back(new Circulo);
            else if(nome == "retangulo")
                X.formas.push_back(new Retangulo);
            else if(nome == "triangulo")
                X.formas.push_back(new Triangulo);
            else if(nome == "disco")
                X.formas.push_back(new Disco);
            else if(nome == "esfera")
                X.formas.push_back(new Esfera);
            else if(nome == "cubo")
                X.formas.push_back(new Cubo);
            else
            {
                cout << "Forma nao reconhecida\n";
                erro = 1;
            }

            if(erro == 0)
            {
                cin >> X.formas[n];
                n++;
                system("pause");
            }
        }

        else if(escolha == 2)
        {
            cout << "\n";
            X.desenha_todas();
            cout << "\n";
            system("pause");
            cout << "\n";
        }

        else if(escolha == 3)
        {
            cout << "\nDigite um ponto contido na forma a ser movida ('x y z', com uma casa decimal):\n";
            cin >> p0.x >> p0.y >> p0.z;
            cout << "Para qual ponto a forma deve ser movida ('x y z', com uma casa decimal)?\n";
            cin >> p.x >> p.y >> p.z;
            cout << "\n";
            X.move_uma(p0, p);
            system("pause");
        }

        else if(escolha == 4)
        {
            cout << "\nPara qual ponto as formas devem ser movidas ('x y z', com uma casa decimal)?\n";
            cin >> p.x >> p.y >> p.z;
            cout << "\n";
            X.move_todas(p);
            system("pause");
        }

        else if(escolha == 5)
        {
            cout << "\nDigite um ponto contido na forma que se quer conhecer a area ('x y z', com uma casa decimal):\n";
            cin >> p.x >> p.y >> p.z;
            cout << "\n";
            X.area_uma(p);
            system("pause");
        }

        else if(escolha == 6)
        {
            cout << "Digite um ponto contido na forma que se quer conhecer o volume ('x y z', com uma casa decimal):\n";
            cin >> p.x >> p.y >> p.z;
            cout << "\n";
            X.volume_uma(p);
            system("pause");
        }

        else if(escolha == 7)
        {
            cout << "\nEm ordem de criacao:\n";
            X.area_todas();
            system("pause");
            cout << "\n\n";
        }

        else if(escolha == 8)
        {
            cout << "\nEm ordem de criacao:\n";
            X.volume_todas();
            system("pause");
            cout << "\n";
        }

        else if(escolha == 9)
            break;

        else
            cout << "\nComando invalido\n";
    }

    return 0;
}


