#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <vector>
#include <fstream>
#include "TP2_1.h"

using namespace std;

double distancia(const ponto& p1, const ponto& p2)
{
    double dist = pow((pow((p1.x - p2.x), 2) + pow((p1.y - p2.y), 2) + pow((p1.z - p2.z), 2)), 0.5);
    return dist;
}

void operator <<(ostream& op, Forma* F)
{
    F->escreve(op);
}

void operator >>(istream& op, Forma* F)
{
    F->le(op);
}

ponto operator +(const ponto& pp1, const ponto& pp2)
{
    ponto aux;
    aux.x = pp1.x + pp2.x;
    aux.y = pp1.y + pp2.y;
    aux.z = pp1.z + pp2.z;
    return aux;
}

ponto operator -(const ponto& pp1, const ponto& pp2)
{
    ponto aux;
    aux.x = pp1.x - pp2.x;
    aux.y = pp1.y - pp2.y;
    aux.z = pp1.z - pp2.z;
    return aux;
}

ostream& operator <<(ostream& op, const ponto& p)
{
    op << "(" << p.x << ", " << p.y << ", " << p.z << ")";
    return op;
}

Minhas_formas::~Minhas_formas()
{
    int aux = (int)formas.size() - 1;
    int i;
    for(i = aux; i >= 0; i--)
    {
        cout << "Destruindo forma...\n";
        delete formas[i];
    }
}

void Minhas_formas::desenha_todas() const
{
    int i = 0;
    for(i = 0; i < (int)formas.size(); i++)
        formas[i]->desenha();
}

void Minhas_formas::move_uma(const ponto& p1, const ponto& p2)
{
    int i = 0;
    for(i = 0; i < (int)formas.size(); i++)
        if(formas[i]->pontoNaForma(p1))
            formas[i]->move(p2);
}

void Minhas_formas::move_todas(const ponto& p)
{
    int i = 0;
    for(i = 0; i < (int)formas.size(); i++)
        formas[i]->move(p);
}

void Minhas_formas::area_todas() const
{
    int i = 0;
    for(i = 0; i < (int)formas.size(); i++)
        cout << formas[i]->area() << endl;
}

void Minhas_formas::volume_todas() const
{
    int i = 0;
    for(i = 0; i < (int)formas.size(); i++)
        cout << formas[i]->volume() << endl;
}

void Minhas_formas::area_uma(const ponto& p) const
{
    int i = 0;
    for(i = 0; i < (int)formas.size(); i++)
        if(formas[i]->pontoNaForma(p))
        {
            cout << "forma " << i+1 << ":" << endl;
            cout << formas[i]->area() << endl;
            cout << endl;
        }
}

void Minhas_formas::volume_uma(const ponto& p) const
{
    int i = 0;
    for(i = 0; i < (int)formas.size(); i++)
        if(formas[i]->pontoNaForma(p))
        {
            cout << "forma " << i+1 << ":" << endl;
            cout << formas[i]->volume() << endl;
            cout << endl;
        }
}

Forma::Forma()
{

}

Forma::~Forma()
{

}

Forma1D::Forma1D()
{

}

Forma1D::~Forma1D()
{

}

Forma2D::Forma2D()
{

}

Forma2D::~Forma2D()
{

}

Forma3D::Forma3D()
{

}

Forma3D::~Forma3D()
{

}

Linha::Linha(ponto pp1, ponto pp2)
{
    p1 = pp1;
    p2 = pp2;
}

Linha::~Linha()
{
    p1 = 0;
    p2 = 0;
}

void Linha::move(const ponto& p)
{
    p2 = p2 - p1 + p;
    p1 = p;
}

void Linha::desenha() const
{
    cout << "Linha com inicio em " << p1 << " e fim em " << p2 << endl;
}

bool Linha::pontoNaForma(const ponto& p) const
{
    /*vetor diretor da reta*/
    ponto diretor = p2 - p1;

    /*vetor que vai de p1 at� p*/
    ponto pp1 = p - p1;

    /*equa��es param�tricas da reta (considerando 0 <= t <= 1)*/
    if(pp1.x/diretor.x == pp1.y/diretor.y)
    {
        if(pp1.x/diretor.x == pp1.z/diretor.z)
        {
            if(pp1.y/diretor.y == pp1.z/diretor.z)
            {
                if(pp1.x/diretor.x >= 0 && pp1.x/diretor.x <= 1)
                    return true;
                else
                    return false;
            }
            else
                return false;
        }
        else
            return false;
    }
    else
        return false;
}

istream& Linha::le(istream& op)
{
    cout << "Digite as coordenadas do primeiro ponto da linha ('x y z', com uma casa decimal): " << endl;
    op >> p1.x >> p1.y >> p1.z;
    cout << "Digite as coordenadas do segundo ponto da linha ('x y z', com uma casa decimal): " << endl;
    op >> p2.x >> p2.y >> p2.z;
    return op;
}

ostream& Linha::escreve(ostream& op){
    op << "Linha definida por " << p1 << " e " << p2 << endl;
    return op;
}

Circulo::Circulo(ponto oo, double rr)
{
    origem = oo;
    raio = rr;
}

Circulo::~Circulo()
{
    origem = 0;
    raio = 0;
}

void Circulo::move(const ponto& p)
{
    origem = p;
}

void Circulo::desenha() const{
    cout << "Circulo centrado em ";
    cout << origem << " de raio " << raio << endl;
}

bool Circulo::pontoNaForma(const ponto& p) const
{
    if(distancia(origem, p) == raio && p.z == origem.z)
        return true;
    else
        return false;
}

istream& Circulo::le(istream& op)
{
    cout << "Digite as coordenadas do centro ('x y z', com uma casa decimal): " << endl;
    op >> origem.x >> origem.y >> origem.z;
    cout << "Defina o raio do circulo: (uma casa decimal)" << endl;
    op >> raio;
    return op;
}

ostream& Circulo::escreve(ostream& op)
{
    op << "Circulo centrado em " << origem << " de raio " << raio << endl;
    return op;
}

Retangulo::Retangulo(ponto pp1, double b, double h)
{
    p1 = pp1;
    base = b;
    altura = h;
}

Retangulo::~Retangulo()
{
    p1 = 0;
    base = 0;
    altura = 0;
}

void Retangulo::move(const ponto& p)
{
    p1 = p;
}

void Retangulo::desenha() const
{
    cout << "Retangulo com diagonal inferior esquerda em " << p1 << " com base " << base << " e altura " << altura << endl;
}

bool Retangulo::pontoNaForma(const ponto& p) const
{
    if(p.x >= p1.x && p.x <= p1.x + base && p.z == p1.z)
    {
        if(p.y >= p1.y && p.y <= p1.y + altura)
            return true;
        else
            return false;
    }
    else
        return false;
}

istream& Retangulo::le(istream& op)
{
    cout << "Digite as coordenadas do ponto inferior esquerdo ('x y z', com uma casa decimal): " << endl;
    op >> p1.x >> p1.y >> p1.z;
    cout << "Defina a base e a altura do retangulo ('base altura', com uma casa decimal): " << endl;
    op >> base >> altura;
    return op;
}

ostream& Retangulo::escreve(ostream& op)
{
    op << "Retangulo com ponto inferior esquerdo em " << p1 << " com base " << base << " e altura " << altura << endl;
    return op;
}

Triangulo::Triangulo(ponto pp1, ponto pp2, ponto pp3)
{
    p1 = pp1;
    p2 = pp2;
    p3 = pp3;
}

Triangulo::~Triangulo()
{
    p1 = 0;
    p2 = 0;
    p3 = 0;
}

void Triangulo::move(const ponto& pp)
{
    p2 = (pp + (p2-p1));
    p3 = (pp + (p3-p1));
    p1 = pp;
}

void Triangulo::desenha() const
{
    cout << "Triangulo definido pelos pontos " << p1 << ", " << p2 << " e " << p3 << endl;
}

double Triangulo::area() const
{
    /*vetores paralelos ao plano do tri�ngulo*/
    ponto V = p2 - p1;
    ponto W = p3 - p1;

    /*m�dulo do produto vetorial*/
    double pvetorial = sqrt(pow(V.y*W.z - V.z*W.y, 2) + pow(V.x*W.z - V.z*W.x, 2) + pow(V.x*W.y - V.y*W.x, 2));

    return pvetorial/2;
}

bool Triangulo::pontoNaForma(const ponto& p) const
{
    Triangulo T1(p1, p2, p), T2(p2, p3, p), T3(p1, p3, p);
    double areat = area();
    double area3 = (T1.area() + T2.area() + T3.area());
    cout << " \n";
    if(areat == area3)
        return true;
    else
        return false;
}

istream& Triangulo::le(istream& op)
{
    cout << "Digite as coordenadas do primeiro ponto do triangulo ('x y z', com uma casa decimal): " << endl;
    op >> p1.x >> p1.y >> p1.z;
    cout << "Digite as coordenadas do segundo ponto do triangulo ('x y z', com uma casa decimal): " << endl;
    op >> p2.x >> p2.y >> p2.z;
    cout << "Digite as coordenadas do terceiro ponto do triangulo ('x y z', com uma casa decimal): " << endl;
    op >> p3.x >> p3.y >> p3.z;
    return op;
}

ostream& Triangulo::escreve(ostream& op)
{
    op << "Triangulo definido por " << p1 << ", " << p2 << " e " << p3 << endl;
    return op;
}

Disco::Disco(ponto oo, double rr)
{
    origem = oo;
    raio = rr;
}

Disco::~Disco()
{
    origem = 0;
    raio = 0;
}

void Disco::move(const ponto& p)
{
    origem = p;
}

void Disco::desenha() const
{
    cout << "Disco centrado em " << origem << " de raio " << raio << endl;
}

bool Disco::pontoNaForma(const ponto& p) const
{
    if(distancia(origem, p) <= raio && p.z == origem.z)
        return true;
    else
        return false;
}

double Disco::area() const
{
    return PI*pow(raio, 2);
}

istream& Disco::le(istream& op)
{
    cout << "Digite as coordenadas do centro ('x y z', com uma casa decimal): " << endl;
    op >> origem.x >> origem.y >> origem.z;
    cout << "Defina o raio do disco (uma casa decimal): " << endl;
    op >> raio;
    return op;
}

ostream& Disco::escreve(ostream& op)
{
    op << "Disco centrado em " << origem << " de raio " << raio << endl;
    return op;
}

Esfera::Esfera(ponto oo, double rr)
{
    origem = oo;
    raio = rr;
}

Esfera::~Esfera()
{
    origem = 0;
    raio = 0;
}

void Esfera::move(const ponto& p)
{
    origem = p;
}

void Esfera::desenha() const
{
    cout << "Esfera centrada em " << origem << " de raio " << raio << endl;
}

double Esfera::area() const
{
    return 4*PI*pow(raio, 2);
}

double Esfera::volume() const
{
    return (4/3)*PI*pow(raio, 3);
}

bool Esfera::pontoNaForma(const ponto& p) const
{
    if(distancia(origem, p) <= raio)
        return true;
    else
        return false;
}

istream& Esfera::le(istream& op)
{
    cout << "Digite as coordenadas do centro ('x y z', com uma casa decimal): " << endl;
    op >> origem.x >> origem.y >> origem.z;
    cout << "Defina o raio da esfera (uma casa decimal): " << endl;
    op >> raio;
    return op;
}

ostream& Esfera::escreve(ostream& op)
{
    op << "Esfera centrada em " << origem << " de raio " << raio << endl;
    return op;
}

Cubo::Cubo(ponto p, double l)
{
    p1 = p;
    lado = l;
}

Cubo::~Cubo()
{
    p1 = 0;
    lado = 0;
}

void Cubo::move(const ponto& p)
{
    p1 = p;
}

void Cubo::desenha() const
{
    cout << "Cubo com diagonal inferior esquerda em " << p1 << " de lado " << lado << endl;
}

double Cubo::area() const
{
    return 6*pow(lado, 2);
}

double Cubo::volume() const
{
    return pow(lado, 3);
}

bool Cubo::pontoNaForma(const ponto& p) const
{
    if(p.x >= p1.x && p.x <= p1.x + lado)
    {
        if(p.y >= p1.y && p.y <= p1.y + lado)
        {
            if(p.z >= p1.z && p.z <= p1.z + lado)
                return true;
            else
                return false;
        }
        else
            return false;
    }
    else
        return false;
}

istream& Cubo::le(istream& op)
{
    cout << "Digite as coordenadas do ponto inferior esquerdo ('x y z', com uma casa decimal): " << endl;
    op >> p1.x >> p1.y >> p1.z;
    cout << "Defina o lado do cubo (uma casa decimal): " << endl;
    op >> lado;
    return op;
}

ostream& Cubo::escreve(ostream& op)
{
    op << "Cubo com ponto inferior esquerdo em " << p1 << " de lado " << lado << endl;
    return op;
}

