#ifndef TP2_1_H_INCLUDED
#define TP2_1_H_INCLUDED

using namespace std;

static const double PI = 3.14;

class ponto
{
public:
    double x, y, z;
    ponto(int xx = 0, int yy = 0, int zz = 0) : x(xx), y(yy), z(zz) {}
};

ponto operator +(const ponto&, const ponto&);
ponto operator -(const ponto&, const ponto&);
ostream& operator <<(ostream&, const ponto&);

class Forma
{
public:
    Forma();
    virtual ~Forma();
    virtual void move(const ponto&) = 0;
    virtual void desenha() const = 0;
    virtual double area() const = 0;
    virtual double volume() const = 0;
    virtual bool pontoNaForma(const ponto&) const = 0;
    virtual void tipo() const = 0;
    virtual istream& le(istream&) = 0;
    virtual ostream& escreve(ostream&) = 0;
};

double distancia(const ponto&, const ponto&);

void operator <<(ostream&, const Forma*);
void operator >>(istream&, Forma*);

class Forma1D : public Forma
{
public:
    Forma1D();
    virtual ~Forma1D();
    void tipo() const { cout << "Forma 1D" << endl; }
    double area() const { return 0; }
    double volume() const { return 0; }
    virtual void move(const ponto&) = 0;
    virtual void desenha() const = 0;
    virtual bool pontoNaForma(const ponto&) const = 0;
    virtual istream& le(istream&) = 0;
    virtual ostream& escreve(ostream&) = 0;
};

class Forma2D : public Forma
{
public:
    Forma2D();
    virtual ~Forma2D();
    void tipo() const { cout << "Forma 2D" << endl; }
    double volume() const { return 0; }
    virtual double area() const = 0;
    virtual void move(const ponto&) = 0;
    virtual void desenha() const = 0;
    virtual bool pontoNaForma(const ponto&) const = 0;
    virtual istream& le(istream&) = 0;
    virtual ostream& escreve(ostream&) = 0;
};

class Forma3D : public Forma
{
public:
    Forma3D();
    virtual ~Forma3D();
    void tipo() const { cout << "Forma 3D" << endl; }
    virtual double area() const = 0;
    virtual double volume() const = 0;
    virtual void move(const ponto&) = 0;
    virtual void desenha() const = 0;
    virtual bool pontoNaForma(const ponto&) const = 0;
    virtual istream& le(istream&) = 0;
    virtual ostream& escreve(ostream&) = 0;
};

class Linha : public Forma1D
{
    ponto p1, p2;
public:
    ~Linha();
    Linha(ponto = 0, ponto = 0);
    void move(const ponto&);
    void desenha() const;
    bool pontoNaForma(const ponto&) const;
    istream& le(istream&);
    ostream& escreve(ostream&);
};

class Circulo : public Forma1D
{
    ponto origem;
    double raio;
public:
    ~Circulo();
    Circulo(ponto oo = 0, double rr = 0);
    void move(const ponto&);
    void desenha() const;
    bool pontoNaForma(const ponto&) const;
    istream& le(istream&);
    ostream& escreve(ostream&);
};

class Retangulo : public Forma2D
{
    ponto p1;
    double base, altura;
public:
    ~Retangulo();
    Retangulo(ponto p1 = 0, double b = 0, double h = 0);
    void move(const ponto&);
    void desenha() const;
    bool pontoNaForma(const ponto&) const;
    double area() const { return base*altura; }
    istream& le(istream&);
    ostream& escreve(ostream&);
};

class Triangulo : public Forma2D
{
    ponto p1, p2, p3;
public:
    ~Triangulo();
    Triangulo(ponto pp1 = 0, ponto pp2 = 0, ponto pp3 = 0);
    void move(const ponto& pp);
    void desenha() const;
    bool pontoNaForma(const ponto&) const;
    double area() const;
    istream& le(istream&);
    ostream& escreve(ostream&);
};

class Disco : public Forma2D
{
    ponto origem;
    double raio;
public:
    ~Disco();
    Disco(ponto oo = 0, double rr = 0);
    void move(const ponto&);
    void desenha() const;
    bool pontoNaForma(const ponto&) const;
    double area() const;
    istream& le(istream&);
    ostream& escreve(ostream&);
};

class Esfera : public Forma3D
{
    ponto origem;
    double raio;
public:
    ~Esfera();
    Esfera(ponto oo = 0, double rr = 0);
    void move(const ponto&);
    void desenha() const;
    bool pontoNaForma(const ponto&) const;
    double area() const;
    double volume() const;
    istream& le(istream&);
    ostream& escreve(ostream&);
};

class Cubo : public Forma3D
{
    ponto p1;
    double lado;
public:
    ~Cubo();
    Cubo(ponto p = 0, double l = 0);
    void move(const ponto&);
    void desenha() const;
    bool pontoNaForma(const ponto&) const;
    double area() const;
    double volume() const;
    istream& le(istream&);
    ostream& escreve(ostream&);
};

class Minhas_formas
{
public:
    vector<Forma*> formas;
    ~Minhas_formas();
    void desenha_todas() const;
    void move_uma(const ponto&, const ponto&);
    void move_todas(const ponto&);
    void area_todas() const;
    void volume_todas() const;
    void area_uma(const ponto&) const;
    void volume_uma(const ponto&) const;
};

#endif // TP2_1_H_INCLUDED

