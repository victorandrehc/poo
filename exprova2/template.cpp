#include <iostream>
using namespace std;

//função Template
template <class T>
 T soma (T a,T b){
 	return a+b;
 }

//classe template
template <class G=string>
 class X
 {
 	G a,b;
 public:
 	X(G aa, G bb){
 		a=aa;
 		b=bb;
 	};
 	G getA(){return a;}
 	G getB(){return b;}
 	
 };


int main(){
	// chamada da função Template int e double implicito e da função int explicito
	cout<<soma(1,2)<<endl<<soma(1.5,2.0)<<endl<<soma<int>(1.5,2)<<endl;
	X<int> x(0,-1);
	cout<<"chamadas da classe int \n"<<x.getA()<<endl<<x.getB()<<endl;
	X <double> xx(0.2,-1.4);
	cout<<"chamadas da classe double\n"<<xx.getA()<<endl<<xx.getB()<<endl;
	X<> xxx("ah","danadinho");
	cout<<"chamadas da classe default\n"<<xxx.getA()<<endl<<xxx.getB()<<endl;
	return 0;
}