#include <iostream>
using namespace std;

class Err{
	string err;
public:
	Err(string e){err=e;};
	void out(){cout<<err<<endl;}
	//~Err();
};
class NotQuiteErr{
	string err;
public:
	NotQuiteErr(string e){err=e;};
	void out(){cout<<err<<endl;}
	//~Err();
};

int divide(int a, int b){
	if(b==0){
		throw Err("Divisao por zero");
	}else if(b==1){
		throw NotQuiteErr("Divisao por 1");
	}
	return a/b;
}
int main(){
	int *a=new int [5];
	try{
		cout<<divide(4,0)<<endl;
	}catch (Err &e){
		e.out();
	}catch (NotQuiteErr &e){
		e.out();
	}
	for(int i=0;i<5;i++){
		a[i]=i;
		cout<<*(a+i);
	}
}

