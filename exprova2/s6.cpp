#include <iostream>
using namespace std;

class Err{
	string err;
public:
	Err(string e){err=e;};
	void out(){cout<<err<<endl;}
	//~Err();
};
template <class V>
class Vector{
	int _size;
	V * parray;
public:
	Vector(int size=1){
		_size=size;
		parray=new V[size];
		for(int i=0;i<_size;i++){
			parray[i]=i;
		}
	};
	~Vector(){
		cout<<"chamando destrutor\n";
		delete [] parray;
	}
	V& operator [](int index){
		if(index<_size && index>=0){
			return parray[index];
		}else{
			throw Err("Indice desformatado");
		}
	}
	void set(V obj,int index){
		if(index<_size && index>=0){
			parray[index]=obj;
		}else{
			throw Err("Indice desformatado");
		}
	}
	int size(){
		return _size;
	}
	Vector & operator= (Vector &);
	bool operator==(Vector &v){
		if(_size!=v._size){
			return false;
		}else{
			for(int i=0;i<_size;i++){
				//cout<<"comparacao "<<i<<" "<<parray[i]<<" "<<v.parray[i]<<endl;
				if(parray[i]!=v.parray[i]){
					return false;
				}
			}
		}
		return true;
	}

};
template <class V>
Vector<V> & Vector<V>::operator= (Vector &a){
	_size=a._size;
	for(int i=0;i<_size;i++){
		parray[i]=a.parray[i];
	}
	return *this;
}

int main(){
	Vector <int> vetor(5);
	vetor.set(-1,0);
	cout<< vetor[0]<<endl;
	Vector <int> vetor2(5);
	vetor2=vetor;
	cout<<vetor[0]<<vetor2[0]<<endl;
	cout<<"igualdade"<<(vetor2==vetor)<<endl;
	//vetor.set(0,0);
	cout<<vetor2[0]<<endl;
	cout<<"sao iguais "<<(vetor2==vetor)<<endl;
	return 0;
}
