#include <iostream>
using namespace std;
class Base {
	public:
	Base(){Teste();}
	virtual void Teste(){cout<<"Base"<<endl;}
	void Chama(){Teste();}
};
class Derivada: public Base{
	public:
	void Teste(){cout<<"Derivada"<<endl;}
};
int main(){
	Derivada d;
	cout<<"construiu"<<endl;
	d.Chama();
}